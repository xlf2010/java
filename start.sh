#!/bin/bash

[ "$#" -lt "1" ] && echo "please input folder " && exit 1

cd "$1"
profile=""
if [ -n "$2" ] 
then
	profile="$profile -Dspring-boot.run.profiles=$2"
fi
curr_pwd=`pwd`
echo "current foler : ${curr_pwd} runing command : mvn spring-boot:run ${profile}"
mvn spring-boot:run ${profile}

package com.harris.spring_boot_study;

public class User {
	
	private Integer FUserId;
	private String FUsername;
	private String FPassword;

	public Integer getFUserId() {
		return FUserId;
	}

	public void setFUserId(Integer fUserId) {
		FUserId = fUserId;
	}

	public String getFUsername() {
		return FUsername;
	}

	public void setFUsername(String fUsername) {
		FUsername = fUsername;
	}

	public String getFPassword() {
		return FPassword;
	}

	public void setFPassword(String fPassword) {
		FPassword = fPassword;
	}

}

package com.harris.spring_boot_study;

import javax.annotation.Resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */

@SpringBootApplication
@RestController
//@ComponentScan("com.harris")
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
public class App {

	@Resource
	private UserMapper userMapper;

	@RequestMapping("/")
	@Transactional
	public String rest(Integer id) {
		System.out.println("param is " + id);
		User user = userMapper.select(id);
		String uname = user.getFUsername();
		user.setFUsername(id + uname);
		user.setFPassword("password" + id);
		System.out.println("insert row affect :" + userMapper.save(user));
		System.out
				.println("update row affect :" + userMapper.update(uname, id));
		return "Hello " + uname;
	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}

package com.harris.spring_boot_study;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.transaction.annotation.Transactional;

@Mapper
public interface UserMapper {
	@Select("select * from test.t_user where F_user_id=#{id}")
	@Results(value = { @Result(property = "FUserId", column = "F_user_id"),
			@Result(property = "FUsername", column = "F_username"),
			@Result(property = "FPassword", column = "F_password") })
	public User select(@Param("id") Integer id);

	@Insert("insert into test.t_user (F_username,F_password) values(#{user.FUsername},#{user.FPassword})")
	public Integer save(@Param("user") User user);

	@Update("update test.t_user set F_username = #{uname} where F_user_id=#{id}")
	@Options(useGeneratedKeys = true)
	public Integer update(@Param("uname") String uname, @Param("id") Integer id);
}

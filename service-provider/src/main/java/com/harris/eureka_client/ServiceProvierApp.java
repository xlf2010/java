package com.harris.eureka_client;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */
//@EnableDiscoveryClient
@SpringBootApplication
@RestController
public class ServiceProvierApp {

	@RequestMapping("/get")
	public String hello(HttpServletRequest request) throws InterruptedException {
		Thread.sleep(10 * 1000);
		System.out.println("remote ip:" + request.getRemoteAddr());
		return "hello";
	}

	public static void main(String[] args) {
		SpringApplication.run(ServiceProvierApp.class, args);
	}
}

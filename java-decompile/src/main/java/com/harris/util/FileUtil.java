package com.harris.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileUtil {
	public static byte[] readFile(String filename) {
		byte[] bs = null;
		try (FileInputStream fis = new FileInputStream(filename)) {
			bs = new byte[fis.available()];
			fis.read(bs);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bs;
	}
}

package com.harris.util;

public class NumberUtil {
	public static short negativeByteChange(byte b) {
		short res = b;
		if (b < 0) {
			res = (short) (b & 0XFF);
		}
		return res;
	}

	public static int negativeShortChange(short s) {
		int res = s;
		if (s < 0) {
			res = s & 0XFFFF;
		}
		return res;
	}

	public static long negativeIntChange(int i) {
		long res = i;
		if (i < 0) {
			res = i & 0XFFFFFFFF;
		}
		return res;
	}

	public static void main(String[] args) {
		short b = -12345;
		System.out.println(negativeShortChange(b));
	}
}

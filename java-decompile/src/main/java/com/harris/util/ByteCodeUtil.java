package com.harris.util;

import com.harris.constant.bytecode.ComparisonsByteCode;
import com.harris.constant.bytecode.ConstantByteCode;

public class ByteCodeUtil {
	private static final int NONE_OPERAND = 0;

	private static final int ONE_BYTE_OPERAND = 1;

	private static final int TOW_BYTE_OPERAND = 2;

	public static int getOperandLen(short opCode) {
		//if比较的后面跟着两位操作数ComparisonsByteCode
		if (opCode >= ComparisonsByteCode.min && opCode <= ComparisonsByteCode.max) {
			return TOW_BYTE_OPERAND;
		}
		//常数操作ConstantByteCode
		if (opCode >= ConstantByteCode.min && opCode <= ConstantByteCode.max) {
			//_dconst_1之前没有操作数
			if(opCode <=ConstantByteCode._dconst_1) {
				return NONE_OPERAND;
			}
			//_ldc之前1字节操作数
			if(opCode <=ConstantByteCode._ldc) {
				return ONE_BYTE_OPERAND;
			}
			//后面为2字节操作数
			return TOW_BYTE_OPERAND;
		}
		
		
		
		//ControlByteCode
		return NONE_OPERAND;
	}
}

package com.harris.util;

import java.util.Arrays;

public class ByteReadUtil {
	private static int currentPos = 0;

	public static byte read1Byte(byte[] bs) {
		checkRange(bs.length, 1);
		byte b = bs[currentPos];
		currentPos++;
		return b;
	}

	public static short read2Byte(byte[] bs) {
		checkRange(bs.length, 2);
		short s = (short) ((bs[currentPos] & 0xFF) << 8 | (bs[currentPos + 1] & 0xFF));
		currentPos += 2;
		return s;
	}

	public static int read4Byte(byte[] bs) {
		checkRange(bs.length, 4);
		int res = 0;
		for (int i = 0; i < 4; i++) {
			res <<= 8;
			res |= (bs[currentPos + i] & 0XFF);
		}
		currentPos += 4;
		return res;
	}

	public static long read8Byte(byte[] bs) {
		checkRange(bs.length, 8);
		long res = 0;
		for (int i = 0; i < 8; i++) {
			res <<= 8;
			res |= (bs[currentPos + i] & 0XFF);
		}
		currentPos += 8;
		return res;
	}

	public static short read1BytePositive(byte[] bs) {
		byte b = read1Byte(bs);
		return b < 0 ? NumberUtil.negativeByteChange(b) : b;
	}

	public static int read2BytePositive(byte[] bs) {
		short s = read2Byte(bs);
		return s < 0 ? NumberUtil.negativeShortChange(s) : s;
	}

	public static long read4BytePositive(byte[] bs) {
		int res = read4Byte(bs);
		return res < 0 ? NumberUtil.negativeIntChange(res) : res;
	}

	private static void checkRange(int maxIndex, int needLength) {
		if (maxIndex < currentPos + needLength) {
			throw new RuntimeException(
					String.format("read byte out of range ,current_pos is %d,need more %d bytes, byte max index is %d",
							currentPos, needLength, maxIndex));
		}
	}

	public static byte[] readNByte(byte[] bs, int n) {
		checkRange(bs.length, n);
		byte[] copy = Arrays.copyOfRange(bs, currentPos, currentPos + n);
		currentPos += n;
		return copy;
	}
	
	public static int getCurrentPos() {
		return currentPos;
	}

	public static void main(String[] args) {

		byte[] bs = "1awfeqwer23rfafwef8sd".getBytes();
		for (int i = 0; i < bs.length; i++) {
			System.out.printf("%x,", bs[i]);
		}
		System.out.println();
		currentPos = 0;
		byte[] s = readNByte(bs, 4);
		for (int i = 0; i < s.length; i++) {
			System.out.printf("%x,", s[i]);
		}
		bs[0] = (byte) 255;
		System.out.printf("\n%x,", read1Byte(bs));
		System.out.printf("\n%x,", read1Byte(bs));
		System.out.printf("\n%x,", read2Byte(bs));
		System.out.printf("\n%x,", read4Byte(bs));
		System.out.printf("\n%x,", read8Byte(bs));
		currentPos = 0;
		System.out.printf("\n%d,", read1Byte(bs));
		System.out.printf("\n%d,", read1Byte(bs));
		System.out.printf("\n%d,", read2Byte(bs));
		System.out.printf("\n%d,", read4Byte(bs));
		System.out.printf("\n%d,", read8Byte(bs));
	}
}

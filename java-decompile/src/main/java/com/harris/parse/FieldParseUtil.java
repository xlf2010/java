package com.harris.parse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.ConstantValueAttrtube;
import com.harris.attrtube.DeprecatedAttrtube;
import com.harris.attrtube.RuntimeInvisibleAnnotationsAttrtube;
import com.harris.attrtube.RuntimeVisibleAnnotationsAttrtube;
import com.harris.attrtube.SignatureAttrtube;
import com.harris.attrtube.SyntheticAttrtube;
import com.harris.constant.pool.CONSTANT_Utf8_info;
import com.harris.info.FieldInfo;
import com.harris.util.ByteReadUtil;

public class FieldParseUtil {

	public static List<FieldInfo> parserFieldInfo(byte[] bs, int fieldCount) {
		if (fieldCount <= 0) {
			return new ArrayList<>();
		}
		ArrayList<FieldInfo> fieldInfos = new ArrayList<FieldInfo>(fieldCount);
		for (int i = 0; i < fieldCount; i++) {
			FieldInfo fieldInfo = new FieldInfo();
			fieldInfo.accessFlag = ByteReadUtil.read2BytePositive(bs);
			fieldInfo.nameIndex = ByteReadUtil.read2BytePositive(bs);
			fieldInfo.descriptionIndex = ByteReadUtil.read2BytePositive(bs);
			fieldInfo.attrtubeCount = ByteReadUtil.read2BytePositive(bs);
			fieldInfo.attrtubeInfos = parseAttrtubeInfos(bs, fieldInfo.attrtubeCount);
			fieldInfos.add(fieldInfo);
		}
		return fieldInfos;
	}

	private static Map<String, AttrtubeInfo> parseAttrtubeInfos(byte[] bs, int attrtubeCount) {
		Map<String, AttrtubeInfo> map = new HashMap<>();
		for (int i = 0; i < attrtubeCount; i++) {
			int nameIndex = ByteReadUtil.read2BytePositive(bs);
			long attrtubeLength = ByteReadUtil.read4BytePositive(bs);
			CONSTANT_Utf8_info utf8_info = (CONSTANT_Utf8_info) ConstantPoolParseUtil.getConstantPoolContents()
					.get(nameIndex);
			String attrtubeName = new String(utf8_info.getBytes());
			AttrtubeInfo attrtubeInfo = null;
			switch (attrtubeName) {
			case AttrtubeInfo.ConstantValue:
				attrtubeInfo = new ConstantValueAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Synthetic:
				attrtubeInfo = new SyntheticAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Signature:
				attrtubeInfo = new SignatureAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Deprecated:
				attrtubeInfo = new DeprecatedAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeVisibleAnnotations:
				attrtubeInfo = new RuntimeVisibleAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeInvisibleAnnotations:
				attrtubeInfo = new RuntimeInvisibleAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			// TODO
			case AttrtubeInfo.RuntimeVisibleTypeAnnotations:

				break;
			case AttrtubeInfo.RuntimeInvisibleTypeAnnotations:

				break;
			default:
				throw new RuntimeException("unsupport attrtubeName:" + attrtubeName);
			}
			attrtubeInfo.fillAttrtube(bs);
			map.put(attrtubeName, attrtubeInfo);
		}
		return map;
	}

}

package com.harris.parse;

import java.util.ArrayList;
import java.util.List;

import com.harris.info.ParameterAnnotationInfo;
import com.harris.util.ByteReadUtil;

public class ParameterAnnotationParseUtil {
	public static List<ParameterAnnotationInfo> parseParameterAnnotationInfo(byte[] bs, int num_parameters) {
		ArrayList<ParameterAnnotationInfo> annotations = new ArrayList<ParameterAnnotationInfo>(num_parameters);
		if (num_parameters <= 0) {
			return annotations;
		}

		for (int i = 0; i < num_parameters; i++) {
			ParameterAnnotationInfo parameterAnnotationInfo = new ParameterAnnotationInfo();
			parameterAnnotationInfo.num_annotations = ByteReadUtil.read2BytePositive(bs);
			parameterAnnotationInfo.annotations = AnnotationParseUtil.parseAnnotation(bs,
					parameterAnnotationInfo.num_annotations);
			annotations.add(parameterAnnotationInfo);
		}
		return annotations;
	}
}

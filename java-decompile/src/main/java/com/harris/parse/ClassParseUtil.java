package com.harris.parse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.BootstrapMethodsAttrtube;
import com.harris.attrtube.DeprecatedAttrtube;
import com.harris.attrtube.EnclosingMethodAttrtube;
import com.harris.attrtube.InnerClassesAttrtube;
import com.harris.attrtube.RuntimeInvisibleAnnotationsAttrtube;
import com.harris.attrtube.RuntimeVisibleAnnotationsAttrtube;
import com.harris.attrtube.SignatureAttrtube;
import com.harris.attrtube.SourceDebugExtensionAttrtube;
import com.harris.attrtube.SourceFileAttrtube;
import com.harris.attrtube.SyntheticAttrtube;
import com.harris.constant.pool.CONSTANT_Utf8_info;
import com.harris.info.ClassInfo;
import com.harris.util.ByteReadUtil;

public class ClassParseUtil {

	public static ClassInfo parserClass(byte[] bs, int versionNum) {
		ClassInfo classInfo = new ClassInfo();
		classInfo.versionNum = versionNum;
		classInfo.accessFlag = ByteReadUtil.read2BytePositive(bs);
		classInfo.thisClassIndex = ByteReadUtil.read2BytePositive(bs);
		classInfo.superClassIndex = ByteReadUtil.read2BytePositive(bs);
		int interfaceCount = ByteReadUtil.read2BytePositive(bs);
		classInfo.interfaceCount = interfaceCount;
		classInfo.interfaceInfos = new ArrayList<>(classInfo.interfaceCount);
		if (interfaceCount > 0) {
			for (int i = 0; i < interfaceCount; i++) {
				classInfo.interfaceInfos.add(ByteReadUtil.read2BytePositive(bs));
			}
		}
		// 字段
		classInfo.fieldInfoCount = ByteReadUtil.read2BytePositive(bs);
		classInfo.fieldInfos = FieldParseUtil.parserFieldInfo(bs, classInfo.fieldInfoCount);
		// 方法
		classInfo.methodCount = ByteReadUtil.read2BytePositive(bs);
		classInfo.methodInfos = MethodParseUtil.parseMethodInfo(bs, classInfo.methodCount);
		// 属性
		classInfo.attrtubeCount = ByteReadUtil.read2BytePositive(bs);
		classInfo.attrtubeInfos = parseAttrtubeInfo(bs, classInfo.attrtubeCount);
		return classInfo;
	}

	public static Map<String, AttrtubeInfo> parseAttrtubeInfo(byte[] bs, int attrtubeCount) {
		Map<String, AttrtubeInfo> map = new HashMap<>();
		for (int i = 0; i < attrtubeCount; i++) {

			int nameIndex = ByteReadUtil.read2BytePositive(bs);
			long attrtubeLength = ByteReadUtil.read4BytePositive(bs);
			CONSTANT_Utf8_info utf8_info = (CONSTANT_Utf8_info) ConstantPoolParseUtil.getConstantPoolContents()
					.get(nameIndex);
			String attrtubeName = new String(utf8_info.getBytes());
			AttrtubeInfo attrtubeInfo = null;
			switch (attrtubeName) {
			case AttrtubeInfo.InnerClasses:
				attrtubeInfo = new InnerClassesAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.EnclosingMethod:
				attrtubeInfo = new EnclosingMethodAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Synthetic:
				attrtubeInfo = new SyntheticAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Signature:
				attrtubeInfo = new SignatureAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.SourceFile:
				attrtubeInfo = new SourceFileAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.SourceDebugExtension:
				attrtubeInfo = new SourceDebugExtensionAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Deprecated:
				attrtubeInfo = new DeprecatedAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeVisibleAnnotations:
				attrtubeInfo = new RuntimeVisibleAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeInvisibleAnnotations:
				attrtubeInfo = new RuntimeInvisibleAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			// TODO
			case AttrtubeInfo.RuntimeVisibleTypeAnnotations:

				break;
			case AttrtubeInfo.RuntimeInvisibleTypeAnnotations:

				break;
			case AttrtubeInfo.BootstrapMethods:
				attrtubeInfo = new BootstrapMethodsAttrtube(nameIndex, attrtubeLength);
			default:
				throw new RuntimeException("unsupport attrtubeName:" + attrtubeName);
			}
			attrtubeInfo.fillAttrtube(bs);
			map.put(attrtubeName, attrtubeInfo);
		}
		return map;
	}

}

package com.harris.parse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.harris.attrtube.AnnotationDefaultAttrtube;
import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.CodeAttrtube;
import com.harris.attrtube.DeprecatedAttrtube;
import com.harris.attrtube.ExceptionsAttrtube;
import com.harris.attrtube.MethodParametersAttrtube;
import com.harris.attrtube.RuntimeInvisibleAnnotationsAttrtube;
import com.harris.attrtube.RuntimeInvisibleParameterAnnotationsAttrtube;
import com.harris.attrtube.RuntimeVisibleAnnotationsAttrtube;
import com.harris.attrtube.RuntimeVisibleParameterAnnotationsAttrtube;
import com.harris.attrtube.SignatureAttrtube;
import com.harris.attrtube.SyntheticAttrtube;
import com.harris.constant.pool.CONSTANT_Utf8_info;
import com.harris.info.MethodInfo;
import com.harris.util.ByteReadUtil;

public class MethodParseUtil {
//	method_info {
//		 u2 access_flags;
//		 u2 name_index;	#UTF8常量索引
//		 u2 descriptor_index;	 #UTF8常量索引
//		 u2 attributes_count;
//		 attribute_info attributes[attributes_count];	#属性
//	}
	public static List<MethodInfo> parseMethodInfo(byte[] bs, int methodCount) {
		if (methodCount <= 0) {
			return new ArrayList<MethodInfo>();
		}
		ArrayList<MethodInfo> methodInfos = new ArrayList<MethodInfo>(methodCount);
		for (int i = 0; i < methodCount; i++) {
			MethodInfo methodInfo = new MethodInfo();
			methodInfo.accessFlag = ByteReadUtil.read2BytePositive(bs);
			methodInfo.nameIndex = ByteReadUtil.read2BytePositive(bs);
			methodInfo.descriptorIndex = ByteReadUtil.read2BytePositive(bs);
			methodInfo.attributesCount = ByteReadUtil.read2BytePositive(bs);
			methodInfo.attrtubeInfos = parseAttrtubeInfos(bs, methodInfo.attributesCount);
			methodInfos.add(methodInfo);
		}
		return methodInfos;
	}

	private static Map<String, AttrtubeInfo> parseAttrtubeInfos(byte[] bs, int attrtubeCount) {
		HashMap<String, AttrtubeInfo> map = new HashMap<>();
		if (attrtubeCount <= 0) {
			return map;
		}

		for (int i = 0; i < attrtubeCount; i++) {
			int nameIndex = ByteReadUtil.read2BytePositive(bs);
			long attrtubeLength = ByteReadUtil.read4BytePositive(bs);
			CONSTANT_Utf8_info utf8_info = (CONSTANT_Utf8_info) ConstantPoolParseUtil.getConstantPoolContents()
					.get(nameIndex);
			String attrtubeName = new String(utf8_info.getBytes());
			AttrtubeInfo attrtubeInfo = null;
			switch (attrtubeName) {
			case AttrtubeInfo.Code:
				attrtubeInfo = new CodeAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Exceptions:
				attrtubeInfo = new ExceptionsAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Synthetic:
				attrtubeInfo = new SyntheticAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Signature:
				attrtubeInfo = new SignatureAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.Deprecated:
				attrtubeInfo = new DeprecatedAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeVisibleAnnotations:
				attrtubeInfo = new RuntimeVisibleAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeInvisibleAnnotations:
				attrtubeInfo = new RuntimeInvisibleAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			// TODO
			case AttrtubeInfo.RuntimeVisibleParameterAnnotations:
				attrtubeInfo = new RuntimeVisibleParameterAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeInvisibleParameterAnnotations:
				attrtubeInfo = new RuntimeInvisibleParameterAnnotationsAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.RuntimeVisibleTypeAnnotations:
				break;
			case AttrtubeInfo.RuntimeInvisibleTypeAnnotations:
				break;
			case AttrtubeInfo.AnnotationDefault:
				attrtubeInfo = new AnnotationDefaultAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.MethodParameters:
				attrtubeInfo = new MethodParametersAttrtube(nameIndex, attrtubeLength);
				break;
			default:
				throw new RuntimeException("unsupport attrtubeName:" + attrtubeName);
			}
			attrtubeInfo.fillAttrtube(bs);
			map.put(attrtubeName, attrtubeInfo);
		}

		return map;
	}
}

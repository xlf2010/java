package com.harris.parse;

import java.util.ArrayList;
import java.util.List;

import com.harris.info.TypeAnnotationInfo;
import com.harris.info.TypeAnnotationInfo.EmptyTarget;
import com.harris.info.TypeAnnotationInfo.FormalParameterTarget;
import com.harris.info.TypeAnnotationInfo.LocalVarTable;
import com.harris.info.TypeAnnotationInfo.LocalvarTarget;
import com.harris.info.TypeAnnotationInfo.OffsetTarget;
import com.harris.info.TypeAnnotationInfo.SupertypeTarget;
import com.harris.info.TypeAnnotationInfo.TargetInfo;
import com.harris.info.TypeAnnotationInfo.ThrowsTarget;
import com.harris.info.TypeAnnotationInfo.TypeArgumentTarget;
import com.harris.info.TypeAnnotationInfo.TypeParameterBoundTarget;
import com.harris.info.TypeAnnotationInfo.TypeParameterTarget;
import com.harris.info.TypeAnnotationInfo.TypePath;
import com.harris.info.TypeAnnotationInfo.TypePathPath;
import com.harris.util.ByteReadUtil;

public class TypeAnnotationParseUtil {
	public static List<TypeAnnotationInfo> parseParameterAnnotationInfos(byte[] bs, int num_annotations) {
		ArrayList<TypeAnnotationInfo> annotationInfos = new ArrayList<>();
		if (num_annotations <= 0) {
			return annotationInfos;
		}

		for (int i = 0; i < num_annotations; i++) {
			TypeAnnotationInfo typeAnnotationInfo = new TypeAnnotationInfo();
			typeAnnotationInfo.target_type = ByteReadUtil.read1BytePositive(bs);
			typeAnnotationInfo.targetInfo = parseTargetInfo(bs, typeAnnotationInfo.target_type);
			typeAnnotationInfo.typePath = parseTypPath(bs);
			typeAnnotationInfo.annotationInfo = AnnotationParseUtil.parseOneAnnotation(bs);
			annotationInfos.add(typeAnnotationInfo);
		}
		return annotationInfos;
	}

	private static TypePath parseTypPath(byte[] bs) {
		TypePath typePath = new TypePath();
		typePath.path_length = ByteReadUtil.read1BytePositive(bs);
		typePath.paths = new ArrayList<>(typePath.path_length);
		if (typePath.path_length <= 0) {
			return typePath;
		}
		for (int i = 0; i < typePath.path_length; i++) {
			TypePathPath typePathPath = new TypePathPath(ByteReadUtil.read1BytePositive(bs),
					ByteReadUtil.read1BytePositive(bs));
			typePath.paths.add(typePathPath);
		}
		return typePath;
	}

	private static TargetInfo parseTargetInfo(byte[] bs, short targeType) {
		TargetInfo targetInfo = new TargetInfo();
		switch (targeType) {

		case TypeAnnotationInfo.TYPE_PARAM_CLASS_INTERFACE:
		case TypeAnnotationInfo.TYPE_PARAM_METHOD_CONSTRUCT:
			targetInfo.typeParameterTarget = new TypeParameterTarget(ByteReadUtil.read1BytePositive(bs));
			break;
		case TypeAnnotationInfo.TYPE_EXTENDS_CLASS_INTERFACE:
			targetInfo.supertypeTarget = new SupertypeTarget(ByteReadUtil.read2BytePositive(bs));
			break;
		case TypeAnnotationInfo.TYPE_BOUND_TYPE_PARAM_CLASS_INTERFACE:
		case TypeAnnotationInfo.TYPE_BOUND_TYPE_PARAM_METHOD_CONSTUACT:
			targetInfo.typeParameterBoundTarget = new TypeParameterBoundTarget(ByteReadUtil.read1Byte(bs),
					ByteReadUtil.read1Byte(bs));
			break;
		case TypeAnnotationInfo.TYPE_FIELD:
		case TypeAnnotationInfo.RETURN_TYPE_TYPE_NEWLY_CONSTRUCT:
		case TypeAnnotationInfo.METHOD_CONSTRUCT_RECEIVER:
			targetInfo.emptyTarget = new EmptyTarget();
			break;
		case TypeAnnotationInfo.METHOD_CONSTRUCT_LAMBDA_FORMAL_PARAMETER:
			targetInfo.methodFormalParameterTarget = new FormalParameterTarget(ByteReadUtil.read1BytePositive(bs));
			break;
		case TypeAnnotationInfo.METHOD_CONSTRUCT_THROW:
			targetInfo.throwsTarget = new ThrowsTarget(ByteReadUtil.read2BytePositive(bs));
			break;

		// code case
		case TypeAnnotationInfo.LOCAL_VARIABLE:
		case TypeAnnotationInfo.RESOURCE_VARIABLE:
			targetInfo.localvarTarget = parseLocalvarTarget(bs);
			break;
		case TypeAnnotationInfo.INSTANCEOF_EXPRESSION:
		case TypeAnnotationInfo.NEW_EXPRESSION:
		case TypeAnnotationInfo.METHOD_REFERENCE_NEW:
		case TypeAnnotationInfo.METHOD_REFERENCE_IDENTIFIER:
			targetInfo.offsetTarget = new OffsetTarget(ByteReadUtil.read2BytePositive(bs));
			break;
		case TypeAnnotationInfo.CAST_EXPRESSION:
		case TypeAnnotationInfo.ARGUEMENT_CONSTRUCT_NEW_INVOCATION:
		case TypeAnnotationInfo.ARGUEMENT_METHOD_INVOCATION:
		case TypeAnnotationInfo.ARGUEMENT_METHOD_REFERENCE_NEW:
		case TypeAnnotationInfo.ARGUEMENT_METHOD_REFERENCE_IDENTIFIER:
			targetInfo.typeArgumentTarget = new TypeArgumentTarget(ByteReadUtil.read2BytePositive(bs),
					ByteReadUtil.read1BytePositive(bs));
			break;
		default:
			break;
		}
		return targetInfo;
	}

	private static LocalvarTarget parseLocalvarTarget(byte[] bs) {
		LocalvarTarget localvarTarget = new LocalvarTarget();
		localvarTarget.table_length = ByteReadUtil.read2BytePositive(bs);
		localvarTarget.localVarTables = new ArrayList<>(localvarTarget.table_length);
		if (localvarTarget.table_length <= 0) {
			return localvarTarget;
		}
		for (int i = 0; i < localvarTarget.table_length; i++) {
			LocalVarTable localVarTable = new LocalVarTable();
			localVarTable.start_pc = ByteReadUtil.read2BytePositive(bs);
			localVarTable.length = ByteReadUtil.read2BytePositive(bs);
			localVarTable.index = ByteReadUtil.read2BytePositive(bs);
			localvarTarget.localVarTables.add(localVarTable);
		}
		return localvarTarget;
	}
}

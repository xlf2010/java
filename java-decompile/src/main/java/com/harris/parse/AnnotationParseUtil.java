package com.harris.parse;

import java.util.ArrayList;
import java.util.List;

import com.harris.info.AnnotationInfo;
import com.harris.info.AnnotationInfo.ArrayValue;
import com.harris.info.AnnotationInfo.ElementValue;
import com.harris.info.AnnotationInfo.ElementValuePair;
import com.harris.info.AnnotationInfo.EnumConstValue;
import com.harris.info.AnnotationInfo.Value;
import com.harris.util.ByteReadUtil;

public class AnnotationParseUtil {
	public static List<AnnotationInfo> parseAnnotation(byte[] bs, int numAnnotations) {
		if (numAnnotations <= 0) {
			return new ArrayList<>();
		}
		ArrayList<AnnotationInfo> annotations = new ArrayList<>(numAnnotations);
		for (int i = 0; i < numAnnotations; i++) {
			annotations.add(parseOneAnnotation(bs));
		}
		return annotations;
	}

	public static AnnotationInfo parseOneAnnotation(byte[] bs) {
		AnnotationInfo annotation = new AnnotationInfo();
		annotation.type_index = ByteReadUtil.read2BytePositive(bs);
		annotation.num_element_value_pairs = ByteReadUtil.read2BytePositive(bs);
		annotation.elementValuePairs = parseElementValuePairs(bs, annotation.num_element_value_pairs);
		return annotation;
	}

	public static List<ElementValuePair> parseElementValuePairs(byte[] bs, int num_element_value_pairs) {
		List<ElementValuePair> elementValuePairs = new ArrayList<>(num_element_value_pairs);
		for (int i = 0; i < num_element_value_pairs; i++) {
			ElementValuePair elementValuePair = new ElementValuePair();
			elementValuePair.element_name_index = ByteReadUtil.read2BytePositive(bs);
			elementValuePair.elementValue = parseElementValue(bs);
			elementValuePairs.add(elementValuePair);
		}
		return elementValuePairs;
	}

	public static ElementValue parseElementValue(byte[] bs) {
		ElementValue elementValue = new ElementValue();
		elementValue.tag = ByteReadUtil.read1BytePositive(bs);
		elementValue.value = parseValue(bs, elementValue.tag);
		return elementValue;
	}

// 		tag值 类型 union联合体值 常量类型
//		B	byte	const_value_index	CONSTANT_Integer
//		C	char	const_value_index	CONSTANT_Integer
//		D	double	const_value_index	CONSTANT_Double
//		F	float	const_value_index	CONSTANT_Float
//		I	int	const_value_index	CONSTANT_Integer
//		J	long	const_value_index	CONSTANT_Long
//		S	short	const_value_index	CONSTANT_Integer
//		Z	boolean	const_value_index	CONSTANT_Integer
//		s	String	const_value_index	CONSTANT_Utf8
//		e	Enum type	enum_const_value	Not applicable
//		c	Class	class_info_index	Not applicable
//		@	Annotation type	annotation_value	Not applicable
//		[	Array type	array_value	Not applicable
	public static Value parseValue(byte[] bs, int tag) {
		Value value = new Value();
		switch (tag) {
		case AnnotationInfo.ELEMENTVALUE_TAG_B:
		case AnnotationInfo.ELEMENTVALUE_TAG_C:
		case AnnotationInfo.ELEMENTVALUE_TAG_D:
		case AnnotationInfo.ELEMENTVALUE_TAG_F:
		case AnnotationInfo.ELEMENTVALUE_TAG_I:
		case AnnotationInfo.ELEMENTVALUE_TAG_J:
		case AnnotationInfo.ELEMENTVALUE_TAG_S:
		case AnnotationInfo.ELEMENTVALUE_TAG_Z:
		case AnnotationInfo.ELEMENTVALUE_TAG_s:
			value.const_value_index = ByteReadUtil.read2BytePositive(bs);
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_e:
			EnumConstValue enumConstValue = new EnumConstValue();
			enumConstValue.type_name_index = ByteReadUtil.read2BytePositive(bs);
			enumConstValue.const_name_index = ByteReadUtil.read2BytePositive(bs);
			value.enum_const_value = enumConstValue;
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_c:
			value.class_info_index = ByteReadUtil.read2BytePositive(bs);
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_ANNOTATION:
			value.annotation_value = parseAnnotation(bs, 1).get(0);
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_ARRAY:
			ArrayValue arrayValue = new ArrayValue();
			arrayValue.num_values = ByteReadUtil.read2BytePositive(bs);
			List<ElementValue> elementValues = new ArrayList<>(arrayValue.num_values);
			if (arrayValue.num_values > 0) {
				for (int i = 0; i < arrayValue.num_values; i++) {
					elementValues.add(parseElementValue(bs));
				}
			}
			arrayValue.values = elementValues;
			value.arrayValue = arrayValue;
			break;
		default:
			break;
		}
		return value;
	}
}

package com.harris.parse;

import java.util.ArrayList;
import java.util.List;

import com.harris.constant.pool.CONSTANT_Class_info;
import com.harris.constant.pool.CONSTANT_Double_info;
import com.harris.constant.pool.CONSTANT_Fieldref_info;
import com.harris.constant.pool.CONSTANT_Float_info;
import com.harris.constant.pool.CONSTANT_Integer_info;
import com.harris.constant.pool.CONSTANT_InterfaceMethodref_info;
import com.harris.constant.pool.CONSTANT_InvokeDynamic_info;
import com.harris.constant.pool.CONSTANT_Long_info;
import com.harris.constant.pool.CONSTANT_MethodHandle_info;
import com.harris.constant.pool.CONSTANT_MethodType_info;
import com.harris.constant.pool.CONSTANT_Methodref_info;
import com.harris.constant.pool.CONSTANT_NameAndType_info;
import com.harris.constant.pool.CONSTANT_String_info;
import com.harris.constant.pool.CONSTANT_Utf8_info;
import com.harris.constant.pool.ConstantPoolContent;
import com.harris.util.ByteReadUtil;

public class ConstantPoolParseUtil {

	public static final int CONSTANT_Class = 7;
	public static final int CONSTANT_Fieldref = 9;
	public static final int CONSTANT_Methodref = 10;
	public static final int CONSTANT_InterfaceMethodref = 11;
	public static final int CONSTANT_String = 8;
	public static final int CONSTANT_MethodType = 16;
	public static final int CONSTANT_MethodHandle = 15;
	public static final int CONSTANT_InvokeDynamic = 18;
	public static final int CONSTANT_Integer = 3;
	public static final int CONSTANT_Float = 4;
	public static final int CONSTANT_Long = 5;
	public static final int CONSTANT_Double = 6;
	public static final int CONSTANT_NameAndType = 12;
	public static final int CONSTANT_Utf8 = 1;

	private static List<ConstantPoolContent> constantPoolContents;

	public static List<ConstantPoolContent> getConstantPoolContents() {
		return constantPoolContents;
	}

	public static String getUtf8String(int constantPooIndex) {
//		CONSTANT_Utf8_info constant_Utf8_info = null;
		ConstantPoolContent constantPoolContent = constantPoolContents.get(constantPooIndex);
		if (constantPoolContent instanceof CONSTANT_Utf8_info) {
			return new String(((CONSTANT_Utf8_info) constantPoolContent).getBytes());
		}
//		try {
//			constant_Utf8_info = (CONSTANT_Utf8_info) constantPoolContent;
//		} catch (Exception e) {
		System.err.println("constant pool index " + constantPooIndex + " not a CONSTANT_Utf8_info");
//			e.printStackTrace();
//		}
//		if (constant_Utf8_info != null) {
//			return new String(constant_Utf8_info.getBytes());
//		}
		return "";
	}

	public static void createConstantPool(byte[] bs) {
		// 读取两字节，负数的话转换成int正数，常量池大小为constantPoolLength-1,索引0位置不用
		int constantPoolLength = ByteReadUtil.read2BytePositive(bs);
		constantPoolContents = new ArrayList<>(constantPoolLength + 1);
		constantPoolContents.add(null);
		for (int i = 1; i < constantPoolLength; i++) {
			ConstantPoolContent constantPoolContent = null;
			int tag = ByteReadUtil.read1BytePositive(bs);
			switch (tag) {
			case CONSTANT_Class:
				constantPoolContent = new CONSTANT_Class_info();
				break;
			case CONSTANT_Fieldref:
				constantPoolContent = new CONSTANT_Fieldref_info();
				break;
			case CONSTANT_Methodref:
				constantPoolContent = new CONSTANT_Methodref_info();
				break;
			case CONSTANT_InterfaceMethodref:
				constantPoolContent = new CONSTANT_InterfaceMethodref_info();
				break;
			case CONSTANT_String:
				constantPoolContent = new CONSTANT_String_info();
				break;
			case CONSTANT_MethodType:
				constantPoolContent = new CONSTANT_MethodType_info();
				break;
			case CONSTANT_MethodHandle:
				constantPoolContent = new CONSTANT_MethodHandle_info();
				break;
			case CONSTANT_InvokeDynamic:
				constantPoolContent = new CONSTANT_InvokeDynamic_info();
				break;
			case CONSTANT_Integer:
				constantPoolContent = new CONSTANT_Integer_info();
				break;
			case CONSTANT_Float:
				constantPoolContent = new CONSTANT_Float_info();
				break;
			case CONSTANT_Long:
				constantPoolContent = new CONSTANT_Long_info();
				break;
			case CONSTANT_Double:
				constantPoolContent = new CONSTANT_Double_info();
				break;
			case CONSTANT_NameAndType:
				constantPoolContent = new CONSTANT_NameAndType_info();
				break;
			case CONSTANT_Utf8:
				constantPoolContent = new CONSTANT_Utf8_info();
				break;
			default:
				break;
			}
			constantPoolContent.fillConstant(bs);
			constantPoolContents.add(constantPoolContent);
		}
	}
}

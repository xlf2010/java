package com.harris.constant.pool;

public interface ConstantPoolContent {
	public int getTag();

	public void fillConstant(byte[] bs);
}

package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_Utf8_info implements ConstantPoolContent {
	private static final int TAG = 1;

	private int length;
	private byte[] bytes;
	private String strInfo;

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
		strInfo = new String(bytes);
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getStrInfo() {
		return strInfo;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		length = ByteReadUtil.read2BytePositive(bs);
		bytes = ByteReadUtil.readNByte(bs, length);
	}

}

package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_Long_info implements ConstantPoolContent {
	private static final int TAG = 5;

	private long longVal;

	public long getLongVal() {
		return longVal;
	}

	public void setLongVal(long longVal) {
		this.longVal = longVal;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		longVal = ByteReadUtil.read8Byte(bs);
	}
}

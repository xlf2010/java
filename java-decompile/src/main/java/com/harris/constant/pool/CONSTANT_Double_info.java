package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_Double_info implements ConstantPoolContent {
	private static final int TAG = 6;

	private double doubleVal;

	public double getDoubleVal() {
		return doubleVal;
	}

	public void setDoubleVal(double doubleVal) {
		this.doubleVal = doubleVal;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		doubleVal = Double.longBitsToDouble(ByteReadUtil.read8Byte(bs));
	}
}

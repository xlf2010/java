package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_Methodref_info implements ConstantPoolContent {
	private static final int TAG = 10;

	private int classIndex;
	private int nameAndTypeIndex;

	public int getClassIndex() {
		return classIndex;
	}

	public void setClassIndex(int classIndex) {
		this.classIndex = classIndex;
	}

	public int getNameAndTypeIndex() {
		return nameAndTypeIndex;
	}

	public void setNameAndTypeIndex(int nameAndTypeIndex) {
		this.nameAndTypeIndex = nameAndTypeIndex;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		classIndex = ByteReadUtil.read2BytePositive(bs);
		nameAndTypeIndex = ByteReadUtil.read2BytePositive(bs);
	}
}

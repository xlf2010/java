package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_Integer_info implements ConstantPoolContent {
	private static final int TAG = 3;

	private int intVal;

	public int getIntVal() {
		return intVal;
	}

	public void setIntVal(int intVal) {
		this.intVal = intVal;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		intVal = ByteReadUtil.read4Byte(bs);
	}
}

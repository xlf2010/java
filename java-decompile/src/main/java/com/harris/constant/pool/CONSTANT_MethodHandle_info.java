package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_MethodHandle_info implements ConstantPoolContent {
	private static final int TAG = 15;
	/**
	 * 必须在[1..9],1..9定义如下备注
	 */
	private int referenceKind;
	/**
	 * # case reference_kind when [1..4] then CONSTANT_Fieldref_info when [5..8]
	 * then CONSTANT_Methodref_info when 9 then CONSTANT_InterfaceMethodref_info
	 */
	private int referenceIndex;

	public int getReferenceIndex() {
		return referenceIndex;
	}

	public void setReferenceIndex(int referenceIndex) {
		this.referenceIndex = referenceIndex;
	}

	public int getReferenceKind() {
		return referenceKind;
	}

	public void setReferenceKind(int referenceKind) {
		this.referenceKind = referenceKind;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		referenceKind = ByteReadUtil.read2BytePositive(bs);
		referenceIndex = ByteReadUtil.read2BytePositive(bs);
	}
}

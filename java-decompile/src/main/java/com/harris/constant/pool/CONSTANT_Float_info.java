package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_Float_info implements ConstantPoolContent {
	private static final int TAG = 4;

	private float floatVal;

	public float getFloatVal() {
		return floatVal;
	}

	public void setFloatVal(float floatVal) {
		this.floatVal = floatVal;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		floatVal = Float.intBitsToFloat(ByteReadUtil.read4Byte(bs));
	}
}

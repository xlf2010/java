package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_MethodType_info implements ConstantPoolContent {
	private static final int TAG = 16;

	private int nameIndex;

	public int getNameIndex() {
		return nameIndex;
	}

	public void setNameIndex(int nameIndex) {
		this.nameIndex = nameIndex;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		nameIndex = ByteReadUtil.read2BytePositive(bs);
	}

}

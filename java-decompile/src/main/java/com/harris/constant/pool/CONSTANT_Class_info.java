package com.harris.constant.pool;

import com.harris.info.TypeInfo;
import com.harris.util.ByteReadUtil;

public class CONSTANT_Class_info implements ConstantPoolContent {
	private static final int TAG = 7;

	/**
	 * UTF8常量索引
	 */
	private int nameIndex;

	public int getNameIndex() {
		return nameIndex;
	}

	public void setNameIndex(int nameIndex) {
		this.nameIndex = nameIndex;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	public String getClassNameSignature() {
		return TypeInfo.getFieldType(nameIndex);
	}

//	public String getClassName() {
//		String signature = ConstantPoolParseUtil.getUtf8String(nameIndex);
//	}

	@Override
	public void fillConstant(byte[] bs) {
		nameIndex = ByteReadUtil.read2BytePositive(bs);
	}
}

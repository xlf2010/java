package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_InvokeDynamic_info implements ConstantPoolContent {
	private static final int TAG = 18;

	private int bootstrapSpecifierIndex;

	private int nameAndTypeIndex;

	public int getBootstrapSpecifierIndex() {
		return bootstrapSpecifierIndex;
	}

	public void setBootstrapSpecifierIndex(int bootstrapSpecifierIndex) {
		this.bootstrapSpecifierIndex = bootstrapSpecifierIndex;
	}

	public int getNameAndTypeIndex() {
		return nameAndTypeIndex;
	}

	public void setNameAndTypeIndex(int nameAndTypeIndex) {
		this.nameAndTypeIndex = nameAndTypeIndex;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		bootstrapSpecifierIndex = ByteReadUtil.read2BytePositive(bs);
		nameAndTypeIndex = ByteReadUtil.read2BytePositive(bs);
	}

}

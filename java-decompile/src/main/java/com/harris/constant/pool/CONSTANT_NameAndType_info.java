package com.harris.constant.pool;

import com.harris.util.ByteReadUtil;

public class CONSTANT_NameAndType_info implements ConstantPoolContent {
	private static final int TAG = 12;

	private int nameIndex;
	private int typeIndex;

	public int getNameIndex() {
		return nameIndex;
	}

	public void setNameIndex(int nameIndex) {
		this.nameIndex = nameIndex;
	}

	public int getTypeIndex() {
		return typeIndex;
	}

	public void setTypeIndex(int typeIndex) {
		this.typeIndex = typeIndex;
	}

	@Override
	public int getTag() {
		return TAG;
	}

	@Override
	public void fillConstant(byte[] bs) {
		nameIndex = ByteReadUtil.read2BytePositive(bs);
		typeIndex = ByteReadUtil.read2BytePositive(bs);
	}
}

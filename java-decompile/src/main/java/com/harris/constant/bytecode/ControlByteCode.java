package com.harris.constant.bytecode;

public class ControlByteCode {
	public static final short _goto                 = 0xa7;
	public static final short _jsr                  = 0xa8;
	public static final short _ret                  = 0xa9;
	public static final short _tableswitch          = 0xaa;
	public static final short _lookupswitch         = 0xab;
	public static final short _ireturn              = 0xac;
	public static final short _lreturn              = 0xad;
	public static final short _freturn              = 0xae;
	public static final short _dreturn              = 0xaf;
	public static final short _areturn              = 0xb0;
	public static final short _return               = 0xb1;
	
	public static final String s_goto          =  "goto"   ;
	public static final String s_jsr          =  "jsr"   ;
	public static final String s_ret          =  "ret"   ;
	public static final String s_tableswitch          =  "tableswitch"   ;
	public static final String s_lookupswitch          =  "lookupswitch"   ;
	public static final String s_ireturn          =  "ireturn"   ;
	public static final String s_lreturn          =  "lreturn"   ;
	public static final String s_freturn          =  "freturn"   ;
	public static final String s_dreturn          =  "dreturn"   ;
	public static final String s_areturn          =  "areturn"   ;
	public static final String s_return          =  "return"   ;
}

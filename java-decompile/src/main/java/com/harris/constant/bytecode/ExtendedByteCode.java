package com.harris.constant.bytecode;

public class ExtendedByteCode {
	public static final short _wide                   = 0xc4;
	public static final short _multianewarray         = 0xc5;
	public static final short _ifnull                 = 0xc6;
	public static final short _ifnonnull              = 0xc7;
	public static final short _goto_w                 = 0xc8;
	public static final short _jsr_w                  = 0xc9;

	public static final String s_wide          =  "wide"   ;
	public static final String s_multianewarray          =  "multianewarray"   ;
	public static final String s_ifnull          =  "ifnull"   ;
	public static final String s_ifnonnull          =  "ifnonnull"   ;
	public static final String s_goto_w          =  "goto_w"   ;
	public static final String s_jsr_w          =  "jsr_w"   ;
}

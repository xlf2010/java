package com.harris.constant.bytecode;

public class StackByteCode {
	public static final short _pop           =   0x57   ;
	public static final short _pop2          =   0x58   ;
	public static final short _dup           =   0x59   ;
	public static final short _dup_x1        =   0x5a   ;
	public static final short _dup_x2        =   0x5b   ;
	public static final short _dup2          =   0x5c   ;
	public static final short _dup2_x1       =   0x5d   ;
	public static final short _dup2_x2       =   0x5e   ;
	public static final short _swap          =   0x5f   ;
	
	
	public static final String s_pop          =  "pop"   ;
	public static final String s_pop2          =  "pop2"   ;
	public static final String s_dup          =  "dup"   ;
	public static final String s_dup_x1          =  "dup_x1"   ;
	public static final String s_dup_x2          =  "dup_x2"   ;
	public static final String s_dup2          =  "dup2"   ;
	public static final String s_dup2_x1          =  "dup2_x1"   ;
	public static final String s_dup2_x2          =  "dup2_x2"   ;
	public static final String s_swap          =  "swap"   ;
}

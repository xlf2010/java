package com.harris.constant.bytecode;

public class ReservedByteCode {
	public static final short _breakposhort      = 0xca; 
	public static final short _impdep1         = 0xfe;
	public static final short _impdep2         = 0xff; 
	
	public static final String s_breakposhort          =  "breakposhort"   ;
	public static final String s_impdep1          =  "impdep1"   ;
	public static final String s_impdep2          =  "impdep2"   ;
}

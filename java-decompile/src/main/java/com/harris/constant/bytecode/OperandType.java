package com.harris.constant.bytecode;

public class OperandType {
	public static final int non = 0;
	public static final int i1 = 1;

	public static final int Methodref = 2;
	public static final int Fieldref = 3;
}

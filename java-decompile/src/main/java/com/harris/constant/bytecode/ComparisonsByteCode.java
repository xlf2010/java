package com.harris.constant.bytecode;

public class ComparisonsByteCode {
	public static final short _lcmp = 0x94;
	public static final short _fcmpl = 0x95;
	public static final short _fcmpg = 0x96;
	public static final short _dcmpl = 0x97;
	public static final short _dcmpg = 0x98;
	public static final short _ifeq = 0x99;
	public static final short _ifne = 0x9a;
	public static final short _iflt = 0x9b;
	public static final short _ifge = 0x9c;
	public static final short _ifgt = 0x9d;
	public static final short _ifle = 0x9e;
	public static final short _if_icmpeq = 0x9f;
	public static final short _if_icmpne = 0xa0;
	public static final short _if_icmplt = 0xa1;
	public static final short _if_icmpge = 0xa2;
	public static final short _if_icmpgt = 0xa3;
	public static final short _if_icmple = 0xa4;
	public static final short _if_acmpeq = 0xa5;
	public static final short _if_acmpne = 0xa6;

	public static final int min = _lcmp;
	public static final int max = _if_acmpne;
	
	
	public static final String s_lcmp 	  = "lcmp";
	public static final String s_fcmpl	  = "fcmpl";
	public static final String s_fcmpg	  = "fcmpg";
	public static final String s_dcmpl	  = "dcmpl";
	public static final String s_dcmpg	  = "dcmpg";
	public static final String s_ifeq 	  = "ifeq";
	public static final String s_ifne 	  = "ifne";
	public static final String s_iflt 	  = "iflt";
	public static final String s_ifge 	  = "ifge";
	public static final String s_ifgt 	  = "ifgt";
	public static final String s_ifle 	  = "ifle";
	public static final String s_if_icmpeq = "if_icmpeq";
	public static final String s_if_icmpne = "if_icmpne";
	public static final String s_if_icmplt = "if_icmplt";
	public static final String s_if_icmpge = "if_icmpge";
	public static final String s_if_icmpgt = "if_icmpgt";
	public static final String s_if_icmple = "if_icmple";
	public static final String s_if_acmpeq = "if_acmpeq";
	public static final String s_if_acmpne = "if_acmpne";

}

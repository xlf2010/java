package com.harris.constant.bytecode;

public class LoadByteCode {
	public static final short _iload      =  0x15       ;
	public static final short _lload      =  0x16       ;
	public static final short _fload      =  0x17       ;
	public static final short _dload      =  0x18       ;
	public static final short _aload      =  0x19       ;
	public static final short _iload_0    =  0x1a       ;
	public static final short _iload_1    =  0x1b       ;
	public static final short _iload_2    =  0x1c       ;
	public static final short _iload_3    =  0x1d       ;
	public static final short _lload_0    =  0x1e       ;
	public static final short _lload_1    =  0x1f       ;
	public static final short _lload_2    =  0x20       ;
	public static final short _lload_3    =  0x21       ;
	public static final short _fload_0    =  0x22       ;
	public static final short _fload_1    =  0x23       ;
	public static final short _fload_2    =  0x24       ;
	public static final short _fload_3    =  0x25       ;
	public static final short _dload_0    =  0x26       ;
	public static final short _dload_1    =  0x27       ;
	public static final short _dload_2    =  0x28       ;
	public static final short _dload_3    =  0x29       ;
	public static final short _aload_0    =  0x2a       ;
	public static final short _aload_1    =  0x2b       ;
	public static final short _aload_2    =  0x2c       ;
	public static final short _aload_3    =  0x2d       ;
	public static final short _iaload     =  0x2e       ;
	public static final short _laload     =  0x2f       ;
	public static final short _faload     =  0x30       ;
	public static final short _daload     =  0x31       ;
	public static final short _aaload     =  0x32       ;
	public static final short _baload     =  0x33       ;
	public static final short _caload     =  0x34       ;
	public static final short _saload     =  0x35       ;
	
	
	public static final String s_iload          =  "iload"   ;
	public static final String s_lload          =  "lload"   ;
	public static final String s_fload          =  "fload"   ;
	public static final String s_dload          =  "dload"   ;
	public static final String s_aload          =  "aload"   ;
	public static final String s_iload_0          =  "iload_0"   ;
	public static final String s_iload_1          =  "iload_1"   ;
	public static final String s_iload_2          =  "iload_2"   ;
	public static final String s_iload_3          =  "iload_3"   ;
	public static final String s_lload_0          =  "lload_0"   ;
	public static final String s_lload_1          =  "lload_1"   ;
	public static final String s_lload_2          =  "lload_2"   ;
	public static final String s_lload_3          =  "lload_3"   ;
	public static final String s_fload_0          =  "fload_0"   ;
	public static final String s_fload_1          =  "fload_1"   ;
	public static final String s_fload_2          =  "fload_2"   ;
	public static final String s_fload_3          =  "fload_3"   ;
	public static final String s_dload_0          =  "dload_0"   ;
	public static final String s_dload_1          =  "dload_1"   ;
	public static final String s_dload_2          =  "dload_2"   ;
	public static final String s_dload_3          =  "dload_3"   ;
	public static final String s_aload_0          =  "aload_0"   ;
	public static final String s_aload_1          =  "aload_1"   ;
	public static final String s_aload_2          =  "aload_2"   ;
	public static final String s_aload_3          =  "aload_3"   ;
	public static final String s_iaload          =  "iaload"   ;
	public static final String s_laload          =  "laload"   ;
	public static final String s_faload          =  "faload"   ;
	public static final String s_daload          =  "daload"   ;
	public static final String s_aaload          =  "aaload"   ;
	public static final String s_baload          =  "baload"   ;
	public static final String s_caload          =  "caload"   ;
	public static final String s_saload          =  "saload"   ;
}

package com.harris.constant.bytecode;

public class OperandLength {
	//switch等不知道操作数长度的值
	public static final int _UNKNOW = -1;
	public static final int _0_BYTE = 0;
	public static final int _1_BYTE = 1;
	public static final int _2_BYTE = 2;
	public static final int _4_BYTE = 4;
	public static final int _8_BYTE = 8;

}

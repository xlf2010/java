package com.harris.constant.bytecode;

public class ConstantByteCode {
	//Constants start
	public static final short _nop               =      0x00     ;
	public static final short _aconst_null       =      0x01     ;
	public static final short _iconst_m1         =      0x02     ;
	public static final short _iconst_0          =      0x03     ;
	public static final short _iconst_1          =      0x04     ;
	public static final short _iconst_2          =      0x05     ;
	public static final short _iconst_3          =      0x06     ;
	public static final short _iconst_4          =      0x07     ;
	public static final short _iconst_5          =      0x08     ;
	public static final short _lconst_0          =      0x09     ;
	public static final short _lconst_1          =      0x0a     ;
	public static final short _fconst_0          =      0x0b     ;
	public static final short _fconst_1          =      0x0c     ;
	public static final short _fconst_2          =      0x0d     ;
	public static final short _dconst_0          =      0x0e     ;
	public static final short _dconst_1          =      0x0f     ;
	public static final short _bipush            =      0x10     ;
	public static final short _sipush            =      0x11     ;
	public static final short _ldc               =      0x12     ;
	public static final short _ldc_w             =      0x13     ;
	public static final short _ldc2_w            =      0x14     ;
	//Constants end
	
	public static final int min = _nop;
	public static final int max = _ldc2_w;
	
	public static final String s_nop          =  "nop"   ;
	public static final String s_aconst_null          =  "aconst_null"   ;
	public static final String s_iconst_m1          =  "iconst_m1"   ;
	public static final String s_iconst_0          =  "iconst_0"   ;
	public static final String s_iconst_1          =  "iconst_1"   ;
	public static final String s_iconst_2          =  "iconst_2"   ;
	public static final String s_iconst_3          =  "iconst_3"   ;
	public static final String s_iconst_4          =  "iconst_4"   ;
	public static final String s_iconst_5          =  "iconst_5"   ;
	public static final String s_lconst_0          =  "lconst_0"   ;
	public static final String s_lconst_1          =  "lconst_1"   ;
	public static final String s_fconst_0          =  "fconst_0"   ;
	public static final String s_fconst_1          =  "fconst_1"   ;
	public static final String s_fconst_2          =  "fconst_2"   ;
	public static final String s_dconst_0          =  "dconst_0"   ;
	public static final String s_dconst_1          =  "dconst_1"   ;
	public static final String s_bipush          =  "bipush"   ;
	public static final String s_sipush          =  "sipush"   ;
	public static final String s_ldc          =  "ldc"   ;
	public static final String s_ldc_w          =  "ldc_w"   ;
	public static final String s_ldc2_w          =  "ldc2_w"   ;
}

package com.harris.constant.bytecode;

public class StackBehavior {

	public static final int push_unknown = -1;

	public static final int pop_unknown = -2;

	public static final int push_0 = 0;

	public static final int push_1 = 1;

	public static final int push_2 = 2;

	public static final int push_i4 = 3;

	public static final int push_f4 = 4;

	public static final int push_l8 = 4;

	public static final int push_d8 = 5;

	public static final int push_a = 6;

	public static final int pop_0 = 10000;

	public static final int pop_1 = 10001;

	public static final int pop_2 = 10002;

	public static final int pop_i4 = 10003;

	public static final int pop_f4 = 10004;

	public static final int pop_l8 = 10004;

	public static final int pop_d8 = 10005;

	public static final int pop_a = 10006;

}

package com.harris.constant.bytecode;

public class ReferencesByteCode {
	public static final short _getstatic            =  0xb2;
	public static final short _putstatic            =  0xb3;
	public static final short _getfield             =  0xb4;
	public static final short _putfield             =  0xb5;
	public static final short _invokevirtual        =  0xb6;
	public static final short _invokespecial        =  0xb7;
	public static final short _invokestatic         =  0xb8;
	public static final short _invokeshorterface      =  0xb9;
	public static final short _invokedynamic        =  0xba;
	public static final short _new                  =  0xbb;
	public static final short _newarray             =  0xbc;
	public static final short _anewarray            =  0xbd;
	public static final short _arraylength          =  0xbe;
	public static final short _athrow               =  0xbf;
	public static final short _checkcast            =  0xc0;
	public static final short _instanceof           =  0xc1;
	public static final short _monitorenter         =  0xc2;
	public static final short _monitorexit          =  0xc3;
	
	
	
	public static final String s_getstatic          =  "getstatic"   ;
	public static final String s_putstatic          =  "putstatic"   ;
	public static final String s_getfield          =  "getfield"   ;
	public static final String s_putfield          =  "putfield"   ;
	public static final String s_invokevirtual          =  "invokevirtual"   ;
	public static final String s_invokespecial          =  "invokespecial"   ;
	public static final String s_invokestatic          =  "invokestatic"   ;
	public static final String s_invokeshorterface          =  "invokeshorterface"   ;
	public static final String s_invokedynamic          =  "invokedynamic"   ;
	public static final String s_new          =  "new"   ;
	public static final String s_newarray          =  "newarray"   ;
	public static final String s_anewarray          =  "anewarray"   ;
	public static final String s_arraylength          =  "arraylength"   ;
	public static final String s_athrow          =  "athrow"   ;
	public static final String s_checkcast          =  "checkcast"   ;
	public static final String s_instanceof          =  "instanceof"   ;
	public static final String s_monitorenter          =  "monitorenter"   ;
	public static final String s_monitorexit          =  "monitorexit"   ;
	
}

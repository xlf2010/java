package com.harris.constant.bytecode;

public class ConversionsByteCode {
	public static final short _i2l =    0x85;
	public static final short _i2f =    0x86;
	public static final short _i2d =    0x87;
	public static final short _l2i =    0x88;
	public static final short _l2f =    0x89;
	public static final short _l2d =    0x8a;
	public static final short _f2i =    0x8b;
	public static final short _f2l =    0x8c;
	public static final short _f2d =    0x8d;
	public static final short _d2i =    0x8e;
	public static final short _d2l =    0x8f;
	public static final short _d2f =    0x90;
	public static final short _i2b =    0x91;
	public static final short _i2c =    0x92;
	public static final short _i2s =    0x93;
	
	
	public static final String s_i2l          =  "i2l"   ;
	public static final String s_i2f          =  "i2f"   ;
	public static final String s_i2d          =  "i2d"   ;
	public static final String s_l2i          =  "l2i"   ;
	public static final String s_l2f          =  "l2f"   ;
	public static final String s_l2d          =  "l2d"   ;
	public static final String s_f2i          =  "f2i"   ;
	public static final String s_f2l          =  "f2l"   ;
	public static final String s_f2d          =  "f2d"   ;
	public static final String s_d2i          =  "d2i"   ;
	public static final String s_d2l          =  "d2l"   ;
	public static final String s_d2f          =  "d2f"   ;
	public static final String s_i2b          =  "i2b"   ;
	public static final String s_i2c          =  "i2c"   ;
	public static final String s_i2s          =  "i2s"   ;
}

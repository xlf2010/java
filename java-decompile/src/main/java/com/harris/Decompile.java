package com.harris;

import com.harris.decompile.ClassDecompile;
import com.harris.info.ClassInfo;
import com.harris.parse.ClassParseUtil;
import com.harris.parse.ConstantPoolParseUtil;
import com.harris.util.ByteReadUtil;
import com.harris.util.FileUtil;

public class Decompile {

	public static void main(String[] args) {
		if (args == null || args.length < 1) {
			System.out.println("please input the file name ");
			return;
		}
		// 1.读取文件
		byte[] bs = FileUtil.readFile(args[0]);
		// 2.校验魔数
		checkMagicNum(bs);
		int versionNum = ByteReadUtil.read4Byte(bs);
		// 3.解析常量池
		ConstantPoolParseUtil.createConstantPool(bs);
		// 4.解析类
		ClassInfo classInfo = ClassParseUtil.parserClass(bs, versionNum);

		ClassDecompile.decompile(classInfo);
	}

	private static void checkMagicNum(byte[] bs) {
		int magicNum = ByteReadUtil.read4Byte(bs);
		if (magicNum != 0xCAFEBABE) {
			throw new RuntimeException("magic num is not 0xCAFEBABE ");
		}
	}
}

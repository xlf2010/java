package com.harris.info;

import java.util.List;

public class TypeAnnotationInfo {
	// target_type
	public static final short TYPE_PARAM_CLASS_INTERFACE = 0X00;
	public static final short TYPE_PARAM_METHOD_CONSTRUCT = 0X01;
	public static final short TYPE_EXTENDS_CLASS_INTERFACE = 0X10;
	public static final short TYPE_BOUND_TYPE_PARAM_CLASS_INTERFACE = 0X11;
	public static final short TYPE_BOUND_TYPE_PARAM_METHOD_CONSTUACT = 0X12;
	public static final short TYPE_FIELD = 0X13;
	public static final short RETURN_TYPE_TYPE_NEWLY_CONSTRUCT = 0X14;
	public static final short METHOD_CONSTRUCT_RECEIVER = 0X15;
	public static final short METHOD_CONSTRUCT_LAMBDA_FORMAL_PARAMETER = 0X16;
	public static final short METHOD_CONSTRUCT_THROW = 0X17;
	// CODE target_type
	public static final short LOCAL_VARIABLE = 0X40;
	public static final short RESOURCE_VARIABLE = 0X41;
	public static final short EXCEPTION_PARAM = 0X42;
	public static final short INSTANCEOF_EXPRESSION = 0X43;
	public static final short NEW_EXPRESSION = 0X44;
	public static final short METHOD_REFERENCE_NEW = 0X45;
	public static final short METHOD_REFERENCE_IDENTIFIER = 0X46;
	public static final short CAST_EXPRESSION = 0X47;
	public static final short ARGUEMENT_CONSTRUCT_NEW_INVOCATION = 0X48;
	public static final short ARGUEMENT_METHOD_INVOCATION = 0X49;
	public static final short ARGUEMENT_METHOD_REFERENCE_NEW = 0X4A;
	public static final short ARGUEMENT_METHOD_REFERENCE_IDENTIFIER = 0X4B;

	public short target_type;
	public TargetInfo targetInfo;
	public TypePath typePath;
	public AnnotationInfo annotationInfo;

	public static class TypeParameterTarget {
		public short type_parameter_index;

		public TypeParameterTarget(short type_parameter_index) {
			this.type_parameter_index = type_parameter_index;
		}
	}

	public static class SupertypeTarget {
		public int supertype_index;

		public SupertypeTarget(int supertype_index) {
			this.supertype_index = supertype_index;
		}
	}

	public static class TypeParameterBoundTarget {
		public short type_parameter_index;
		public short bound_index;

		public TypeParameterBoundTarget(short type_parameter_index, short bound_index) {
			this.type_parameter_index = type_parameter_index;
			this.bound_index = bound_index;
		}
	}

	public static class EmptyTarget {
	}

	public static class FormalParameterTarget {
		public short formal_parameter_index;

		public FormalParameterTarget(short formal_parameter_index) {
			this.formal_parameter_index = formal_parameter_index;
		}
	}

	public static class ThrowsTarget {
		public int throws_type_index;

		public ThrowsTarget(int throws_type_index) {
			this.throws_type_index = throws_type_index;
		}

	}

	public static class LocalVarTable {
		public int start_pc;
		public int length;
		public int index;
	}

	public static class LocalvarTarget {
		public int table_length;
		public List<LocalVarTable> localVarTables;
	}

	public static class CatchTarget {
		public int exception_table_index;

		public CatchTarget(int exception_table_index) {
			this.exception_table_index = exception_table_index;
		}

	}

	public static class OffsetTarget {
		public int offset;

		public OffsetTarget(int offset) {
			this.offset = offset;
		}

	}

	public static class TypeArgumentTarget {
		public int offset;
		public short type_argument_index;

		public TypeArgumentTarget(int offset, short type_argument_index) {
			this.offset = offset;
			this.type_argument_index = type_argument_index;
		}

	}

	public static class TypePathPath {
		public short type_path_kind;
		public short type_argument_index;

		public TypePathPath(short type_path_kind, short type_argument_index) {
			this.type_path_kind = type_path_kind;
			this.type_argument_index = type_argument_index;
		}

	}

	public static class TypePath {
		public short path_length;
		public List<TypePathPath> paths;
	}

	public static class TargetInfo {
		public TypeParameterTarget typeParameterTarget;
		public SupertypeTarget supertypeTarget;
		public TypeParameterBoundTarget typeParameterBoundTarget;
		public EmptyTarget emptyTarget;
		public FormalParameterTarget methodFormalParameterTarget;
		public ThrowsTarget throwsTarget;
		public LocalvarTarget localvarTarget;
		public CatchTarget catchTarget;
		public OffsetTarget offsetTarget;
		public TypeArgumentTarget typeArgumentTarget;
	}

}

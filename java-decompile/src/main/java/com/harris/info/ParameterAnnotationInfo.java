package com.harris.info;

import java.util.List;

public class ParameterAnnotationInfo {
	public int num_annotations;
	public List<AnnotationInfo> annotations;
}

package com.harris.info;

import java.util.List;

public class AnnotationInfo {
	// ElementValue.tag
	// byte
	public static final char ELEMENTVALUE_TAG_B = 'B';
	// char
	public static final char ELEMENTVALUE_TAG_C = 'C';
//	double
	public static final char ELEMENTVALUE_TAG_D = 'D';
	// float
	public static final char ELEMENTVALUE_TAG_F = 'F';
	// int
	public static final char ELEMENTVALUE_TAG_I = 'I';
	// long
	public static final char ELEMENTVALUE_TAG_J = 'J';
	// short
	public static final char ELEMENTVALUE_TAG_S = 'S';
	// boolean
	public static final char ELEMENTVALUE_TAG_Z = 'Z';
	// string
	public static final char ELEMENTVALUE_TAG_s = 's';
	// enum
	public static final char ELEMENTVALUE_TAG_e = 'e';
	// class
	public static final char ELEMENTVALUE_TAG_c = 'c';
	// annotation
	public static final char ELEMENTVALUE_TAG_ANNOTATION = '@';
	// array
	public static final char ELEMENTVALUE_TAG_ARRAY = '[';

	public static class EnumConstValue {
		public int type_name_index;
		public int const_name_index;
	}

	public static class ArrayValue {
		public int num_values;
		public List<ElementValue> values;
	}

	public static class Value {
		public int const_value_index;
		public EnumConstValue enum_const_value;
//		class_info_index表示如下：
//		1. 如果是类名称，接口或数组，则为类型名称，如Object.class表示为Ljava/lang/Object
//		2. 如果是基础类型，则为对应的缩写，如int.class表示为I
//		3. 如果是void，则为V
		public int class_info_index;
		public AnnotationInfo annotation_value;
		public ArrayValue arrayValue;
	}

	public static class ElementValue {
//		tag值	类型	union联合体值	常量类型
//		B	byte	const_value_index	CONSTANT_Integer
//		C	char	const_value_index	CONSTANT_Integer
//		D	double	const_value_index	CONSTANT_Double
//		F	float	const_value_index	CONSTANT_Float
//		I	int	const_value_index	CONSTANT_Integer
//		J	long	const_value_index	CONSTANT_Long
//		S	short	const_value_index	CONSTANT_Integer
//		Z	boolean	const_value_index	CONSTANT_Integer
//		s	String	const_value_index	CONSTANT_Utf8
//		e	Enum type	enum_const_value	Not applicable
//		c	Class	class_info_index	Not applicable
//		@	Annotation type	annotation_value	Not applicable
//		[	Array type	array_value	Not applicable
		public int tag;
		public Value value;
	}

	public static class ElementValuePair {
		public int element_name_index;
		public ElementValue elementValue;
	}

	public int type_index;
	public int num_element_value_pairs;
	public List<ElementValuePair> elementValuePairs;
}

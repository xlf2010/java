package com.harris.info;

import java.util.Map;

import com.harris.attrtube.AttrtubeInfo;

public class FieldInfo {
	// u2
	public int accessFlag;
	// 字段名，utf8_constant
	public int nameIndex;
	// 类型,utf8_constant
	public int descriptionIndex;
	public int attrtubeCount;
	public Map<String, AttrtubeInfo> attrtubeInfos;

	public static final int ACC_PUBLIC = 0x0001;
	public static final int ACC_PRIVATE = 0x0002;
	public static final int ACC_PROTECTED = 0x0004;
	public static final int ACC_STATIC = 0x0008;

	public static final int ACC_FINAL = 0x0010;
	public static final int ACC_VOLATILE = 0x0040;
	public static final int ACC_TRANSIENT = 0x0080;

	public static final int ACC_SYNTHETIC = 0x1000;
	public static final int ACC_ENUM = 0x4000;

	public String getFieldName() {
		return TypeInfo.getFieldName(nameIndex);
	}

	public String getFieldType() {
		return TypeInfo.getFieldType(descriptionIndex);
	}

	public boolean isPublic() {
		return (accessFlag & ACC_PUBLIC) == ACC_PUBLIC;
	}

	public boolean isPrivate() {
		return (accessFlag & ACC_PRIVATE) == ACC_PRIVATE;
	}

	public boolean isProtected() {
		return (accessFlag & ACC_PROTECTED) == ACC_PROTECTED;
	}

	public boolean isStatic() {
		return (accessFlag & ACC_STATIC) == ACC_STATIC;
	}

	public boolean isFinal() {
		return (accessFlag & ACC_FINAL) == ACC_FINAL;
	}

	public boolean isVolatile() {
		return (accessFlag & ACC_VOLATILE) == ACC_VOLATILE;
	}

	public boolean isTransient() {
		return (accessFlag & ACC_TRANSIENT) == ACC_TRANSIENT;
	}

	public boolean isSynthetic() {
		return (accessFlag & ACC_SYNTHETIC) == ACC_SYNTHETIC;
	}

	public boolean isEnum() {
		return (accessFlag & ACC_ENUM) == ACC_ENUM;
	}

}

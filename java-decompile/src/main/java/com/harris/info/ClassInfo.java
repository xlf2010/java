package com.harris.info;

import java.util.List;
import java.util.Map;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.constant.pool.CONSTANT_Class_info;
import com.harris.constant.pool.CONSTANT_Utf8_info;
import com.harris.parse.ConstantPoolParseUtil;

public class ClassInfo {

	public static final int ACC_PUBLIC = 0x0001;
	public static final int ACC_FINAL = 0x0010;
	public static final int ACC_SUPER = 0x0020;
	public static final int ACC_INTERFACE = 0x0200;
	public static final int ACC_ABSTRACT = 0x0400;
	public static final int ACC_SYNTHETIC = 0x1000;
	public static final int ACC_ANNOTATION = 0x2000;
	public static final int ACC_ENUM = 0x4000;

	public int versionNum;
	public int accessFlag = 0;
	public int thisClassIndex = 0;
	public int superClassIndex = 0;
	public int interfaceCount = 0;
	public List<Integer> interfaceInfos;
	public int fieldInfoCount = 0;
	public List<FieldInfo> fieldInfos;
	public int methodCount = 0;
	public List<MethodInfo> methodInfos;
	public int attrtubeCount = 0;
	public Map<String, AttrtubeInfo> attrtubeInfos;

	public String getThisClassName() {
		return getClassName(thisClassIndex);
	}

	public String getSuperClassName() {
		return getClassName(superClassIndex);
	}

	public static String getClassName(int classIndex) {
		CONSTANT_Class_info class_info = (CONSTANT_Class_info) ConstantPoolParseUtil.getConstantPoolContents()
				.get(classIndex);
		CONSTANT_Utf8_info utf8_info = (CONSTANT_Utf8_info) ConstantPoolParseUtil.getConstantPoolContents()
				.get(class_info.getNameIndex());
		return new String(utf8_info.getBytes()).replace("/", ".");
	}

	public boolean isPublic() {
		return (accessFlag & ACC_PUBLIC) == ACC_PUBLIC;
	}

	public boolean isFinal() {
		return (accessFlag & ACC_FINAL) == ACC_FINAL;
	}

	public boolean isSuper() {
		return (accessFlag & ACC_SUPER) == ACC_SUPER;
	}

	public boolean isInterface() {
		return (accessFlag & ACC_INTERFACE) == ACC_INTERFACE;
	}

	public boolean isAbstract() {
		return (accessFlag & ACC_ABSTRACT) == ACC_ABSTRACT;
	}

	public boolean isSynthetic() {
		return (accessFlag & ACC_SYNTHETIC) == ACC_SYNTHETIC;
	}

	public boolean isAnnotation() {
		return (accessFlag & ACC_ANNOTATION) == ACC_ANNOTATION;
	}

	public boolean isEnum() {
		return (accessFlag & ACC_ENUM) == ACC_ENUM;
	}

}

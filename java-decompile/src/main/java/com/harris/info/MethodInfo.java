package com.harris.info;

import java.util.List;
import java.util.Map;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.parse.ConstantPoolParseUtil;
import com.harris.util.StringUtils;

public class MethodInfo {
	public static final int ACC_PUBLIC = 0x0001;
	public static final int ACC_PRIVATE = 0x0002;
	public static final int ACC_PROTECTED = 0x0004;
	public static final int ACC_STATIC = 0x0008;
	public static final int ACC_FINAL = 0x0010;
	public static final int ACC_SYNCHRONIZED = 0x0020;
	public static final int ACC_BRIDGE = 0x0040;
	public static final int ACC_VARARGS = 0x0080;
	public static final int ACC_NATIVE = 0x0100;
	public static final int ACC_ABSTRACT = 0x0400;
	public static final int ACC_STRICT = 0x0800;
	public static final int ACC_SYNTHETIC = 0x1000;

	public int accessFlag;
	public int nameIndex;
	public int descriptorIndex;
	public int attributesCount;
	public Map<String, AttrtubeInfo> attrtubeInfos;

	// 方法参数
	public List<Map<String, String>> methodParameters;
	// 方法返回值
	public String returnTypeName;

	public String getMethodName() {
		return TypeInfo.getFieldName(nameIndex);
	}

	public List<Map<String, String>> getMethodParameters() {
		if (methodParameters == null) {
			String utf8String = ConstantPoolParseUtil.getUtf8String(descriptorIndex);
			methodParameters = TypeInfo.getMethodParameter(utf8String);
		}
		return methodParameters;
	}

	public String getReturnType() {
		if (StringUtils.isNotEmpty(returnTypeName)) {
			return returnTypeName;
		}
		String description = ConstantPoolParseUtil.getUtf8String(descriptorIndex);
		return returnTypeName = TypeInfo.getMethodReturnType(description);
	}

	public boolean isPublic() {
		return (accessFlag & ACC_PUBLIC) == ACC_PUBLIC;
	}

	public boolean isPrivate() {
		return (accessFlag & ACC_PRIVATE) == ACC_PRIVATE;
	}

	public boolean isProtected() {
		return (accessFlag & ACC_PROTECTED) == ACC_PROTECTED;
	}

	public boolean isStatic() {
		return (accessFlag & ACC_STATIC) == ACC_STATIC;
	}

	public boolean isFinal() {
		return (accessFlag & ACC_FINAL) == ACC_FINAL;
	}

	public boolean isSynchornized() {
		return (accessFlag & ACC_SYNCHRONIZED) == ACC_SYNCHRONIZED;
	}

	public boolean isBridge() {
		return (accessFlag & ACC_BRIDGE) == ACC_BRIDGE;
	}

	public boolean isVarargs() {
		return (accessFlag & ACC_VARARGS) == ACC_VARARGS;
	}

	public boolean isNative() {
		return (accessFlag & ACC_NATIVE) == ACC_NATIVE;
	}

	public boolean isAbstract() {
		return (accessFlag & ACC_ABSTRACT) == ACC_ABSTRACT;
	}

	public boolean isStrict() {
		return (accessFlag & ACC_STRICT) == ACC_STRICT;
	}

	public boolean isSynthetic() {
		return (accessFlag & ACC_SYNTHETIC) == ACC_SYNTHETIC;
	}

}

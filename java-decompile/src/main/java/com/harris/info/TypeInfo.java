package com.harris.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.harris.parse.ConstantPoolParseUtil;
import com.harris.util.StringUtils;

public class TypeInfo {
	public static String getFieldName(int nameIndex) {
		return ConstantPoolParseUtil.getUtf8String(nameIndex);
	}

	public static void main(String[] args) {
		String string = getFieldTypeByDescription("[Ljava/lang/Object");
		System.out.println(string);
		string = getFieldTypeByDescription("[[[B");
		System.out.println(string);
	}

	public static String getFieldType(int descriptionIndex) {
		String utf8String = ConstantPoolParseUtil.getUtf8String(descriptionIndex);
		// 解析数组类型
		return getFieldTypeByDescription(utf8String);
	}

//	  获取字段描述符，解析回代码
//	   FieldDescriptor:
//			FieldType
//		FieldType:
//			BaseType 	#基础类型
//			ObjectType 	#引用类型
//			ArrayType	#数组类型
//		BaseType:
//			(one of) 
//			B C D F I J S Z	#基础类型分别为：byte,char,double,float,int,long,short,boolean
//		ObjectType:
//			L ClassName ;
//		ArrayType:
//			[ ComponentType
//		ComponentType:
//			FieldType

	public static String getFieldTypeByDescription(String utf8String) {
		switch (utf8String) {
		case "B":
			return "byte";
		case "C":
			return "char";
		case "D":
			return "double";
		case "F":
			return "float";
		case "I":
			return "int";
		case "J":
			return "long";
		case "Z":
			return "boolean";
		case "V":
			return "void";
		default:
			break;
		}

		// 解析引用类型 Ljava.lang.Object
		if (utf8String.startsWith("L")) {
			return utf8String.substring(1).replace("/", ".").replace(";", "");
		}

		// 解析数组，类似于[[B
		if (utf8String.startsWith("[")) {
			char[] cs = utf8String.toCharArray();
			int arrayDim = 0;
			StringBuilder arraybBuilder = new StringBuilder();
			for (int i = 0; i < cs.length; i++) {
				if (cs[i] != '[') {
					break;
				}
				arrayDim++;
				arraybBuilder.append("[]");
			}
			return getFieldTypeByDescription(new String(cs, arrayDim, utf8String.length() - arrayDim))
					+ arraybBuilder.toString();
		}
		return "";
	}

	public static String getTypeParmaterType(String utf8String) {
		// 普通泛型 如TK;
		if (utf8String.startsWith("T")) {
			return utf8String.substring(1);
		}
		// 数组类型 如 [[TK
		if (utf8String.startsWith("[")) {
			char[] charArray = utf8String.toCharArray();
			StringBuilder sb = new StringBuilder();
			int arrayDim = 0;
			for (int i = 0; i < charArray.length; i++) {
				if (charArray[i] != '[') {
					break;
				}
				arrayDim++;
				sb.append("[]");
			}
			return utf8String.substring(arrayDim + 1, utf8String.length() - 1) + sb.toString();
		}
		return "";
	}

	// MethodDescriptor:
	// ( {ParameterDescriptor} ) ReturnDescriptor
	// ParameterDescriptor: #参数描述
	// FieldType
	// ReturnDescriptor: #返回值描述
	// FieldType
	// VoidDescriptor
	// VoidDescriptor: #void返回值描述
	// V
	public static String getMethodReturnType(String utf8String) {
		return getFieldTypeByDescription(utf8String.split("\\)")[1]);
	}

	public static List<Map<String, String>> getMethodParameter(String utf8String) {
		String[] typeList = utf8String.split("\\)")[0].split(";");
		List<Map<String, String>> list = new ArrayList<>();
		for (int i = 0; i < typeList.length; i++) {
			if (StringUtils.isEmpty(typeList[i])) {
				continue;
			}
			Map<String, String> param = new HashMap<>();
			param.put(getFieldTypeByDescription(typeList[i]), "args" + i);
			list.add(param);
		}
		return list;
	}
}

package com.harris.attrtube;

import java.util.List;

import com.harris.info.TypeAnnotationInfo;
import com.harris.parse.TypeAnnotationParseUtil;
import com.harris.util.ByteReadUtil;

public class RuntimeVisibleTypeAnnotationsAttrtube extends AbstractAttrtubeInfo {

	public int num_annotations;
	public List<TypeAnnotationInfo> annotations;

	public RuntimeVisibleTypeAnnotationsAttrtube() {
	}

	public RuntimeVisibleTypeAnnotationsAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		num_annotations = ByteReadUtil.read2BytePositive(bs);
		annotations = TypeAnnotationParseUtil.parseParameterAnnotationInfos(bs, num_annotations);
	}
}

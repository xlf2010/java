package com.harris.attrtube;

public interface AttrtubeInfo {

	// jvm能正确解析class文件5个属性：
	public static final String ConstantValue = "ConstantValue";
	public static final String Code = "Code";
	public static final String StackMapTable = "StackMapTable";
	public static final String Exceptions = "Exceptions";
	public static final String BootstrapMethods = "BootstrapMethods";

	// java SE类库能正确解析class文件的12个属性
	public static final String InnerClasses = "InnerClasses";
	public static final String EnclosingMethod = "EnclosingMethod";
	public static final String Synthetic = "Synthetic";
	public static final String Signature = "Signature";
	public static final String RuntimeVisibleAnnotations = "RuntimeVisibleAnnotations";
	public static final String RuntimeInvisibleAnnotations = "RuntimeInvisibleAnnotations";
	public static final String RuntimeVisibleParameterAnnotations = "RuntimeVisibleParameterAnnotations";
	public static final String RuntimeInvisibleParameterAnnotations = "RuntimeInvisibleParameterAnnotations";
	public static final String RuntimeVisibleTypeAnnotations = "RuntimeVisibleTypeAnnotations";
	public static final String RuntimeInvisibleTypeAnnotations = "RuntimeInvisibleTypeAnnotations";
	public static final String AnnotationDefault = "AnnotationDefault";
	public static final String MethodParameters = "MethodParameters";

	// 不是jvm或java SE类库必须的属性，但是比较有用的6个属性
	public static final String SourceFile = "SourceFile";
	public static final String SourceDebugExtension = "SourceDebugExtension";
	public static final String LineNumberTable = "LineNumberTable";
	public static final String LocalVariableTable = "LocalVariableTable";
	public static final String LocalVariableTypeTable = "LocalVariableTypeTable";
	public static final String Deprecated = "Deprecated";

	/**
	 * 
	 */
	public int getAttrtubeNameIndex();

	public String getAttrtubeName();

	public long getAttrtubeLength();

	public void fillAttrtube(byte[] bs);
}

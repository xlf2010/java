package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.util.ByteReadUtil;

public class LocalVariableTableAttrtube extends AbstractAttrtubeInfo {
	public static class LocalVariableTable {
		public int startPc;
		public int length;
		public int nameIndex;
		public int descriptorIndex;
		public int index;
	}

	public int localVariableTableLength;
	public List<LocalVariableTable> loVariableTables;

	public LocalVariableTableAttrtube() {
	}

	public LocalVariableTableAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		localVariableTableLength = ByteReadUtil.read2BytePositive(bs);
		loVariableTables = new ArrayList<>(localVariableTableLength);
		if (localVariableTableLength <= 0) {
			return;
		}

		for (int i = 0; i < localVariableTableLength; i++) {
			LocalVariableTable localVariableTable = new LocalVariableTable();
			localVariableTable.startPc = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.length = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.nameIndex = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.descriptorIndex = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.index = ByteReadUtil.read2BytePositive(bs);
			loVariableTables.add(localVariableTable);
		}
	}

}

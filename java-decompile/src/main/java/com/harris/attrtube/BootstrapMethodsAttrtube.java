package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.util.ByteReadUtil;

public class BootstrapMethodsAttrtube extends AbstractAttrtubeInfo {
	public static class BootstrapMethod {
		public int bootstrap_method_ref;
		public int num_bootstrap_arguments;
		// size=num_bootstrap_arguments
		public List<Integer> bootstrap_arguments;
	}

	public int num_bootstrap_methods;
	// size=num_bootstrap_methods
	public List<BootstrapMethod> bootstrapMethods;

	public BootstrapMethodsAttrtube() {
	}

	public BootstrapMethodsAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		num_bootstrap_methods = ByteReadUtil.read2BytePositive(bs);
		bootstrapMethods = new ArrayList<>(num_bootstrap_methods);
		if (num_bootstrap_methods <= 0) {
			return;
		}
		for (int i = 0; i < num_bootstrap_methods; i++) {
			BootstrapMethod bootstrapMethod = new BootstrapMethod();
			bootstrapMethod.bootstrap_method_ref = ByteReadUtil.read2BytePositive(bs);
			bootstrapMethod.num_bootstrap_arguments = ByteReadUtil.read2BytePositive(bs);
			bootstrapMethod.bootstrap_arguments = new ArrayList<>();
			for (int j = 0; j < bootstrapMethod.num_bootstrap_arguments; j++) {
				bootstrapMethod.bootstrap_arguments.add(ByteReadUtil.read2BytePositive(bs));
			}
		}
	}
}

package com.harris.attrtube;

import com.harris.info.AnnotationInfo.ElementValue;
import com.harris.parse.AnnotationParseUtil;

public class AnnotationDefaultAttrtube extends AbstractAttrtubeInfo {

	public ElementValue elementValue;

	public AnnotationDefaultAttrtube() {
	}

	public AnnotationDefaultAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		elementValue = AnnotationParseUtil.parseElementValue(bs);
	}
	
}

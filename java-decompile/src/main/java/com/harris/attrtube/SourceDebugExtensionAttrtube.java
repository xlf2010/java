package com.harris.attrtube;

import java.util.List;

public class SourceDebugExtensionAttrtube extends AbstractAttrtubeInfo {

	// #CONSTANT_Utf8_info
	public List<Integer> debugExtension;

	public SourceDebugExtensionAttrtube() {
	}

	public SourceDebugExtensionAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

}

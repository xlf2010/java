package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.util.ByteReadUtil;

public class StackMapTableAttrtube extends AbstractAttrtubeInfo {
	// frame info
	public static final int max_same_frame = 63;
	public static final int max_same_locals_1_stack_item_frame = 127;
	public static final int max_same_locals_1_stack_item_frame_extended = 247;
	public static final int max_chop_frame = 250;
	public static final int max_same_frame_extended = 251;
	public static final int max_append_frame = 254;
	public static final int max_full_frame = 255;

	// variable_info
	public static final int Top_variable_info = 0;
	public static final int Integer_variable_info = 1;
	public static final int Float_variable_info = 2;
	public static final int Double_variable_info = 3;
	public static final int Long_variable_info = 4;
	public static final int Null_variable_info = 5;
	public static final int UninitializedThis_variable_info = 6;
	public static final int Object_variable_info = 7;
	public static final int Uninitialized_variable_info = 8;

	public int numberOfEntries;
	public List<StackMapFrame> stackMapFrames;

	public StackMapTableAttrtube() {
	}

	public StackMapTableAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	public static class StackMapFrame {

//		  union stack_map_frame {
//			    same_frame;				 0-63 
//			    same_locals_1_stack_item_frame;	 64-127 
//			    same_locals_1_stack_item_frame_extended;	 247 
//			    chop_frame;	 248-250 
//			    same_frame_extended;	  251 
//			    append_frame;	 252-254 
//			    full_frame;	 255 
//			}

		public short frameType;
//		类型为:
//		same_locals_1_stack_item_frame_extended,
//		chop_frame
//		same_frame_extended
//		append_frame
//		full_frame 有的变量
		public int offsetDelta;
//		full_frame,
//		append_frame=frame_type-251
		public int numberOfLocals;
//		full_frame.size=numberOfLocals,
//		append_frame.size=frame_type-251
		public List<VerificationTypeInfo> locals;
//		same_locals_1_stack_item_frame=1
//		same_locals_1_stack_item_frame_extended=1
//		full_frame
		public int numberOfStacks;
//		same_locals_1_stack_item_frame.size=1
//		same_locals_1_stack_item_frame_extended.size=1
//		full_frame.size=numberOfStacks
		public List<VerificationTypeInfo> stacks;
	}

	public static class VerificationTypeInfo {
		public short tag;
		public int cpoolIndex_offset;
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		numberOfEntries = ByteReadUtil.read2BytePositive(bs);
		stackMapFrames = new ArrayList<>(numberOfEntries);
		if (numberOfEntries <= 0) {
			return;
		}
		for (int i = 0; i < numberOfEntries; i++) {
			stackMapFrames.add(parseStackMapFrame(bs));
		}
	}

	private StackMapFrame parseStackMapFrame(byte[] bs) {
		StackMapFrame stackMapFrame = new StackMapFrame();
		stackMapFrame.frameType = ByteReadUtil.read1BytePositive(bs);
		// same frame do nothing; [0,63]

		// same_locals_1_stack_item_frame [64,127]
		if (stackMapFrame.frameType > max_same_frame && stackMapFrame.frameType <= max_same_locals_1_stack_item_frame) {
			stackMapFrame.stacks = parseverificationTypeInfo(bs, 1);
		} else
		// same_locals_1_stack_item_frame_extended=247
		if (stackMapFrame.frameType == max_same_locals_1_stack_item_frame_extended) {
			stackMapFrame.offsetDelta = ByteReadUtil.read2BytePositive(bs);
			stackMapFrame.stacks = parseverificationTypeInfo(bs, 1);
		} else
		// chop_frame [248,250]
		if (stackMapFrame.frameType > max_same_locals_1_stack_item_frame_extended
				&& stackMapFrame.frameType <= max_chop_frame) {
			stackMapFrame.offsetDelta = ByteReadUtil.read2BytePositive(bs);
		} else
//			same_frame_extended =251
		if (stackMapFrame.frameType == max_same_frame_extended) {
			stackMapFrame.offsetDelta = ByteReadUtil.read2BytePositive(bs);
		} else
//			append_frame [252,254]
		if (stackMapFrame.frameType > max_same_frame_extended && stackMapFrame.frameType <= max_append_frame) {
			stackMapFrame.offsetDelta = ByteReadUtil.read2BytePositive(bs);
			stackMapFrame.locals = parseverificationTypeInfo(bs, stackMapFrame.frameType - max_same_frame_extended);
		} else
		// full_frame=255
		if (stackMapFrame.frameType == max_full_frame) {
			stackMapFrame.offsetDelta = ByteReadUtil.read2BytePositive(bs);
			stackMapFrame.numberOfLocals = ByteReadUtil.read2BytePositive(bs);
			stackMapFrame.locals = parseverificationTypeInfo(bs, stackMapFrame.numberOfLocals);
			stackMapFrame.numberOfStacks = ByteReadUtil.read2BytePositive(bs);
			stackMapFrame.stacks = parseverificationTypeInfo(bs, stackMapFrame.numberOfStacks);
		}
		return stackMapFrame;
	}

	private List<VerificationTypeInfo> parseverificationTypeInfo(byte[] bs, int count) {
		ArrayList<VerificationTypeInfo> verificationTypeInfos = new ArrayList<>(count);
		if (count <= 0) {
			return verificationTypeInfos;
		}
		for (int i = 0; i < count; i++) {
			VerificationTypeInfo typeInfo = new VerificationTypeInfo();
			typeInfo.tag = ByteReadUtil.read1BytePositive(bs);

			// Object_variable_info=7 cpoolIndex
			// Uninitialized_variable_info=8 ,offset
			if (typeInfo.tag == Object_variable_info || typeInfo.tag == Uninitialized_variable_info) {
				typeInfo.cpoolIndex_offset = ByteReadUtil.read2BytePositive(bs);
			}
			verificationTypeInfos.add(typeInfo);
		}
		return verificationTypeInfos;
	}

}

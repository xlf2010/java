package com.harris.attrtube;

import java.util.List;

import com.harris.info.AnnotationInfo;
import com.harris.parse.AnnotationParseUtil;
import com.harris.util.ByteReadUtil;

public class RuntimeVisibleAnnotationsAttrtube extends AbstractAttrtubeInfo {

	public int numAnnotations;
	public List<AnnotationInfo> annotations;

	public RuntimeVisibleAnnotationsAttrtube() {
	}

	public RuntimeVisibleAnnotationsAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		numAnnotations = ByteReadUtil.read2BytePositive(bs);
		annotations = AnnotationParseUtil.parseAnnotation(bs, numAnnotations);
	}
}

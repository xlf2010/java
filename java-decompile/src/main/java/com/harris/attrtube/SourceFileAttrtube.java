package com.harris.attrtube;

import com.harris.util.ByteReadUtil;

public class SourceFileAttrtube extends AbstractAttrtubeInfo {

	// #CONSTANT_Utf8_info
	public int sourcefileIndex;

	public SourceFileAttrtube() {
	}

	public SourceFileAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		sourcefileIndex = ByteReadUtil.read2BytePositive(bs);
	}

}

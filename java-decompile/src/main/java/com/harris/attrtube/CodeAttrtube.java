package com.harris.attrtube;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.harris.constant.bytecode.ControlByteCode;
import com.harris.constant.bytecode.OperandLength;
import com.harris.constant.pool.CONSTANT_Utf8_info;
import com.harris.decompile.ast.instruction.ByteCodeInstruction;
import com.harris.decompile.ast.instruction.OpCode;
import com.harris.parse.ConstantPoolParseUtil;
import com.harris.util.ByteReadUtil;

public class CodeAttrtube extends AbstractAttrtubeInfo {

	public static class ExceptionTable {
		public int startPc;
		public int endPc;
		public int handlerPc;
		public int catchType;
	}

	public int maxStack;
	public int maxLocals;
	public long codeLength;
	public List<ByteCodeInstruction> codes;
	public int exceptionTableLength;
	public List<ExceptionTable> exceptionTables;
	public int attrtubeCount;
	public Map<String, AttrtubeInfo> attrtubeInfos;

	public CodeAttrtube() {
	}

	public CodeAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		maxStack = ByteReadUtil.read2BytePositive(bs);
		maxLocals = ByteReadUtil.read2BytePositive(bs);
		codeLength = ByteReadUtil.read4BytePositive(bs);
		if (codeLength > 0) {
			// java 虚拟机规范为4字节无符号数，如果是转成int为负数，则会报错
			int codeLen = (int) codeLength;
			codes = getByteCodeInstruction(bs, codeLen);
		}
		exceptionTableLength = ByteReadUtil.read2BytePositive(bs);
		exceptionTables = parseExceptionTables(bs, exceptionTableLength);
		attrtubeCount = ByteReadUtil.read2BytePositive(bs);
		attrtubeInfos = parseCodeInnerAttrtube(bs, attrtubeCount);
	}

	private static List<ByteCodeInstruction> getByteCodeInstruction(byte[] bs, int codeLength) {
		List<ByteCodeInstruction> list = new ArrayList<>(codeLength);
		for (int i = 0; i < codeLength; i++) {
			ByteCodeInstruction byteCodeInstruction = new ByteCodeInstruction();
			short code = ByteReadUtil.read1BytePositive(bs);
			byteCodeInstruction.offset = i;
			byteCodeInstruction.opCode = OpCode.OP_CODES[code];
			// switch unknown length
			if (byteCodeInstruction.opCode.operandLength == OperandLength._UNKNOW) {
				handleUnKnownOperandLength(bs, byteCodeInstruction);
			} else if (byteCodeInstruction.opCode.operandLength > 0) {
				byteCodeInstruction.operand = ByteReadUtil.readNByte(bs, byteCodeInstruction.opCode.operandLength);
				byteCodeInstruction.operandLength = byteCodeInstruction.opCode.operandLength;
			}
			i += byteCodeInstruction.operandLength;
			list.add(byteCodeInstruction);
		}
		return list;
	}

	private static void handleUnKnownOperandLength(byte[] bs, ByteCodeInstruction byteCodeInstruction) {
		OpCode opCode = byteCodeInstruction.opCode;
		switch (opCode.opCode) {
		case ControlByteCode._tableswitch:

			break;
		case ControlByteCode._lookupswitch:
			
			break;
		default:
			break;
		}
	}

	private static Map<String, AttrtubeInfo> parseCodeInnerAttrtube(byte[] bs, int attrtubeCount) {
		HashMap<String, AttrtubeInfo> map = new HashMap<>();
		if (attrtubeCount <= 0) {
			return map;
		}

		for (int i = 0; i < attrtubeCount; i++) {
			int nameIndex = ByteReadUtil.read2BytePositive(bs);
			long attrtubeLength = ByteReadUtil.read4BytePositive(bs);
			CONSTANT_Utf8_info utf8_info = (CONSTANT_Utf8_info) ConstantPoolParseUtil.getConstantPoolContents()
					.get(nameIndex);
			String attrtubeName = new String(utf8_info.getBytes());
			AttrtubeInfo attrtubeInfo = null;
			switch (attrtubeName) {
			case AttrtubeInfo.StackMapTable:
				attrtubeInfo = new StackMapTableAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.LineNumberTable:
				attrtubeInfo = new LineNumberTableAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.LocalVariableTable:
				attrtubeInfo = new LocalVariableTableAttrtube(nameIndex, attrtubeLength);
				break;
			case AttrtubeInfo.LocalVariableTypeTable:
				attrtubeInfo = new LocalVariableTypeTableAttrtube(nameIndex, attrtubeLength);
				break;
			default:
				throw new RuntimeException("unsupport attrtube name : " + attrtubeName);
			}
			attrtubeInfo.fillAttrtube(bs);
			map.put(attrtubeName, attrtubeInfo);
		}
		return map;
	}

	private static List<ExceptionTable> parseExceptionTables(byte[] bs, int exceptionTableLength) {
		if (exceptionTableLength <= 0) {
			return new ArrayList<ExceptionTable>();
		}

		List<ExceptionTable> exceptionTables = new ArrayList<>(exceptionTableLength);
		for (int i = 0; i < exceptionTableLength; i++) {
			ExceptionTable exceptionTable = new ExceptionTable();
			exceptionTable.startPc = ByteReadUtil.read2BytePositive(bs);
			exceptionTable.endPc = ByteReadUtil.read2BytePositive(bs);
			exceptionTable.handlerPc = ByteReadUtil.read2BytePositive(bs);
			exceptionTable.catchType = ByteReadUtil.read2BytePositive(bs);
			exceptionTables.add(exceptionTable);
		}
		return exceptionTables;
	}

}

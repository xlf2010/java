package com.harris.attrtube;

import java.util.List;

import com.harris.info.ParameterAnnotationInfo;
import com.harris.parse.ParameterAnnotationParseUtil;
import com.harris.util.ByteReadUtil;

public class RuntimeInvisibleParameterAnnotationsAttrtube extends AbstractAttrtubeInfo {

	public int num_parameters;
	public List<ParameterAnnotationInfo> annotations;

	public RuntimeInvisibleParameterAnnotationsAttrtube() {
	}

	public RuntimeInvisibleParameterAnnotationsAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		num_parameters = ByteReadUtil.read2BytePositive(bs);
		annotations = ParameterAnnotationParseUtil.parseParameterAnnotationInfo(bs, num_parameters);
	}
}

package com.harris.attrtube;

import com.harris.util.ByteReadUtil;

public class SignatureAttrtube extends AbstractAttrtubeInfo {

	// #CONSTANT_Utf8_info
	public int signatureIndex;

	public SignatureAttrtube() {
	}

	public SignatureAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		signatureIndex = ByteReadUtil.read2BytePositive(bs);
	}

}

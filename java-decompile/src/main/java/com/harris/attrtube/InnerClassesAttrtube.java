package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.info.ClassInfo;
import com.harris.util.ByteReadUtil;

public class InnerClassesAttrtube extends AbstractAttrtubeInfo {

	public static class InnerClassInfo extends ClassInfo {

		public static final int ACC_PRIVATE = 0x0002;
		public static final int ACC_PROTECTED = 0x0004;
		public static final int ACC_STATIC = 0x0008;

		public int innerClassInfoIndex;
		public int outerClassInfoIndex;
		public int innerNameIndex;

		public boolean isPrivate() {
			return (accessFlag & ACC_PRIVATE) == ACC_PRIVATE;
		}

		public boolean isProtected() {
			return (accessFlag & ACC_PROTECTED) == ACC_PROTECTED;
		}

		public boolean isStatic() {
			return (accessFlag & ACC_STATIC) == ACC_STATIC;
		}

	}

	public int numberOfClasses;
	public List<InnerClassInfo> innerClassInfos;

	public InnerClassesAttrtube() {
	}

	public InnerClassesAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	public void fillAttrtube(byte[] bs) {
		numberOfClasses = ByteReadUtil.read2BytePositive(bs);
		innerClassInfos = new ArrayList<>(numberOfClasses);
		if (numberOfClasses <= 0) {
			return;
		}
		for (int i = 0; i < numberOfClasses; i++) {
			InnerClassInfo innerClassInfo = new InnerClassInfo();
			innerClassInfo.innerClassInfoIndex = ByteReadUtil.read2BytePositive(bs);
			innerClassInfo.outerClassInfoIndex = ByteReadUtil.read2BytePositive(bs);
			innerClassInfo.innerNameIndex = ByteReadUtil.read2BytePositive(bs);
			innerClassInfo.accessFlag = ByteReadUtil.read2BytePositive(bs);
			innerClassInfos.add(innerClassInfo);
		}
	}
}

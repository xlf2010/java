package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.util.ByteReadUtil;

public class MethodParametersAttrtube extends AbstractAttrtubeInfo {

	public static class Parameter {
		public int name_index;
		public int access_flags;
	}

	public int parameters_count;
	public List<Parameter> parameters;

	public MethodParametersAttrtube() {
	}

	public MethodParametersAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		parameters_count = ByteReadUtil.read2BytePositive(bs);
		parameters = new ArrayList<>(parameters_count);
		if (parameters_count <= 0) {
			return;
		}
		for (int i = 0; i < parameters_count; i++) {
			Parameter parameter = new Parameter();
			parameter.name_index = ByteReadUtil.read2BytePositive(bs);
			parameter.access_flags = ByteReadUtil.read2BytePositive(bs);
			parameters.add(parameter);
		}
	}

}

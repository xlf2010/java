package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.util.ByteReadUtil;

public class LocalVariableTypeTableAttrtube extends AbstractAttrtubeInfo {
	public static class LocalVariableTypeTable {
		public int startPc;
		public int length;
		public int nameIndex;
		public int signatureIndex;
		public int index;
	}

	public int localVariableTypeTableLength;
	public List<LocalVariableTypeTable> loVariableTypeTables;

	public LocalVariableTypeTableAttrtube() {
	}

	public LocalVariableTypeTableAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		localVariableTypeTableLength = ByteReadUtil.read2BytePositive(bs);
		loVariableTypeTables = new ArrayList<>(localVariableTypeTableLength);
		if (localVariableTypeTableLength <= 0) {
			return;
		}

		for (int i = 0; i < localVariableTypeTableLength; i++) {
			LocalVariableTypeTable localVariableTable = new LocalVariableTypeTable();
			localVariableTable.startPc = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.length = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.nameIndex = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.signatureIndex = ByteReadUtil.read2BytePositive(bs);
			localVariableTable.index = ByteReadUtil.read2BytePositive(bs);
			loVariableTypeTables.add(localVariableTable);
		}
	}

}

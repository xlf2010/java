package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.util.ByteReadUtil;

public class LineNumberTableAttrtube extends AbstractAttrtubeInfo {
	public static class LineNumberTable {
		public int startPc;
		public int lineNumber;
	}

	public int lineNumberTableLength;
	public List<LineNumberTable> lineNumberTables;

	public LineNumberTableAttrtube() {
	}

	public LineNumberTableAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		lineNumberTableLength = ByteReadUtil.read2BytePositive(bs);
		lineNumberTables = new ArrayList<>(lineNumberTableLength);
		if (lineNumberTableLength <= 0) {
			return;
		}

		for (int i = 0; i < lineNumberTableLength; i++) {
			LineNumberTable lineNumberTable = new LineNumberTable();
			lineNumberTable.startPc = ByteReadUtil.read2BytePositive(bs);
			lineNumberTable.lineNumber = ByteReadUtil.read2BytePositive(bs);
			lineNumberTables.add(lineNumberTable);
		}
	}
}

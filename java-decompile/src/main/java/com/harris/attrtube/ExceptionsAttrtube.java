package com.harris.attrtube;

import java.util.ArrayList;
import java.util.List;

import com.harris.util.ByteReadUtil;

public class ExceptionsAttrtube extends AbstractAttrtubeInfo {

	public int numberOfExceptions;
	public List<Integer> exceptionIndexTables;

	public ExceptionsAttrtube() {
	}

	public ExceptionsAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		numberOfExceptions = ByteReadUtil.read2BytePositive(bs);
		exceptionIndexTables = new ArrayList<Integer>(numberOfExceptions);
		if (numberOfExceptions <= 0) {
			return;
		}
		for (int j = 0; j < numberOfExceptions; j++) {
			exceptionIndexTables.add(ByteReadUtil.read2BytePositive(bs));
		}
	}

}

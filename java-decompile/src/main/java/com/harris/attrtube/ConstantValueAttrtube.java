package com.harris.attrtube;

import com.harris.util.ByteReadUtil;

public class ConstantValueAttrtube extends AbstractAttrtubeInfo {

	public int constantValueIndex;

	public ConstantValueAttrtube() {
	}

	public ConstantValueAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		constantValueIndex = ByteReadUtil.read2BytePositive(bs);
	}
}

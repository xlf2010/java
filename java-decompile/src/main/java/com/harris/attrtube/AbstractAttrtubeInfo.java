package com.harris.attrtube;

import com.harris.constant.pool.CONSTANT_Utf8_info;
import com.harris.parse.ConstantPoolParseUtil;

public class AbstractAttrtubeInfo implements AttrtubeInfo {

	private int nameIndex;
	private long attrtubeLength;

	public AbstractAttrtubeInfo() {
	}

	public AbstractAttrtubeInfo(int nameIndex, long attrtubeLength) {
		super();
		this.nameIndex = nameIndex;
		this.attrtubeLength = attrtubeLength;
	}

	@Override
	public int getAttrtubeNameIndex() {
		return nameIndex;
	}

	@Override
	public long getAttrtubeLength() {
		return attrtubeLength;
	}

	@Override
	public String getAttrtubeName() {
		CONSTANT_Utf8_info constant_Utf8_info = (CONSTANT_Utf8_info) ConstantPoolParseUtil.getConstantPoolContents()
				.get(nameIndex);
		return new String(constant_Utf8_info.getBytes());
	}

	public void setNameIndex(int nameIndex) {
		this.nameIndex = nameIndex;
	}

	public void setAttrtubeLength(long attrtubeLength) {
		this.attrtubeLength = attrtubeLength;
	}

	@Override
	public void fillAttrtube(byte[] bs) {
	}

}

package com.harris.attrtube;

import com.harris.util.ByteReadUtil;

public class EnclosingMethodAttrtube extends AbstractAttrtubeInfo {

	public int classIndex;
	public int methodIndex;

	public EnclosingMethodAttrtube() {
	}

	public EnclosingMethodAttrtube(int nameIndex, long attrtubeLength) {
		super(nameIndex, attrtubeLength);
	}

	@Override
	public void fillAttrtube(byte[] bs) {
		classIndex = ByteReadUtil.read2BytePositive(bs);
		methodIndex = ByteReadUtil.read2BytePositive(bs);
	}

}

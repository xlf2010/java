package com.harris.decompile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.ConstantValueAttrtube;
import com.harris.attrtube.RuntimeVisibleAnnotationsAttrtube;
import com.harris.attrtube.SignatureAttrtube;
import com.harris.constant.Constant;
import com.harris.constant.pool.CONSTANT_Double_info;
import com.harris.constant.pool.CONSTANT_Float_info;
import com.harris.constant.pool.CONSTANT_Integer_info;
import com.harris.constant.pool.CONSTANT_Long_info;
import com.harris.constant.pool.CONSTANT_String_info;
import com.harris.constant.pool.ConstantPoolContent;
import com.harris.info.ClassInfo;
import com.harris.info.FieldInfo;
import com.harris.info.TypeInfo;
import com.harris.parse.ConstantPoolParseUtil;
import com.harris.util.MapUtil;

public class FieldDecompile {

	private static final String ANNOTATIONS = "annotations";
	private static final String ACCESS_FLAG = "accessFlag";
	private static final String CLASS_TYPE = "type";
	private static final String FIELD_NAME = "fieldName";
	private static final String CONSTANT_VALUE = "constantValue";

	private static final String FIELD_FORMAT = "[annotations][accessFlag][type][fieldName][constantValue]";

	public static String decompile(ClassInfo classInfo) {
		List<FieldInfo> fieldInfos = classInfo.fieldInfos;
		if (fieldInfos == null || fieldInfos.size() <= 0) {
			return "";
		}
		Map<String, String> map = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		for (FieldInfo fieldInfo : fieldInfos) {
			initMap(map);
			getFieldDeclare(map, classInfo, fieldInfo);
			getFileTypeName(map, fieldInfo);
			getAttrtube(map, fieldInfo);
			sb.append(MapUtil.parseMap(map, FIELD_FORMAT)).append(";\n");
		}
		return sb.toString();
	}

	private static void initMap(Map<String, String> map) {
		map.put(ANNOTATIONS, "");
		map.put(ACCESS_FLAG, "");
		map.put(CLASS_TYPE, "");
		map.put(CONSTANT_VALUE, "");
	}

	private static String getAttrtube(Map<String, String> map, FieldInfo fieldInfo) {
		if (fieldInfo.attrtubeCount <= 0) {
			return "";
		}
		for (Entry<String, AttrtubeInfo> entry : fieldInfo.attrtubeInfos.entrySet()) {
			switch (entry.getKey()) {
			case AttrtubeInfo.ConstantValue:
				ConstantValueAttrtube constantValueAttrtube = (ConstantValueAttrtube) entry.getValue();
				MapUtil.putValue(map, CONSTANT_VALUE, "=" + parseConstantValue(map, constantValueAttrtube));
				break;
			case AttrtubeInfo.Synthetic:
				break;
			case AttrtubeInfo.Signature:
				String signature = ConstantPoolParseUtil
						.getUtf8String(((SignatureAttrtube) entry.getValue()).signatureIndex);
				map.put(CLASS_TYPE, " " + TypeInfo.getTypeParmaterType(signature));
				break;
			case AttrtubeInfo.Deprecated:
				break;
			case AttrtubeInfo.RuntimeVisibleAnnotations:
				MapUtil.putValue(map, ANNOTATIONS, AnnotationDecompile
						.decompileAnnotation(((RuntimeVisibleAnnotationsAttrtube) entry.getValue()).annotations));
				break;
			case AttrtubeInfo.RuntimeInvisibleAnnotations:
				MapUtil.putValue(map, ANNOTATIONS, AnnotationDecompile
						.decompileAnnotation(((RuntimeVisibleAnnotationsAttrtube) entry.getValue()).annotations));
				break;
			case AttrtubeInfo.RuntimeVisibleTypeAnnotations:

				break;
			case AttrtubeInfo.RuntimeInvisibleTypeAnnotations:

				break;
			default:
				break;
			}
		}
		return null;
	}

	private static String parseConstantValue(Map<String, String> map, ConstantValueAttrtube constantValueAttrtube) {
		ConstantPoolContent constantPoolContent = ConstantPoolParseUtil.getConstantPoolContents()
				.get(constantValueAttrtube.constantValueIndex);
		if (constantPoolContent instanceof CONSTANT_Long_info) {
			return Long.toString(((CONSTANT_Long_info) constantPoolContent).getLongVal()) + "L";
		}
		if (constantPoolContent instanceof CONSTANT_Float_info) {
			return Float.toString(((CONSTANT_Float_info) constantPoolContent).getFloatVal()) + "F";
		}
		if (constantPoolContent instanceof CONSTANT_Double_info) {
			return Double.toString(((CONSTANT_Double_info) constantPoolContent).getDoubleVal());
		}
//		CONSTANT_Integer_info 包括boolean,short char byte int
		if (constantPoolContent instanceof CONSTANT_Integer_info) {
			int val = ((CONSTANT_Integer_info) constantPoolContent).getIntVal();
			String type = map.get(CLASS_TYPE);
			if (Constant.PRIVARY_TYPE_NAME_BYTE.equals(type)) {
				return Byte.toString((byte) val);
			}
			if (Constant.PRIVARY_TYPE_NAME_BOOLEAN.equals(type)) {
				return 1 == val ? "true" : "false";
			}
			if (Constant.PRIVARY_TYPE_NAME_SHORT.equals(type)) {
				return Short.toString((short) val);
			}
			if (Constant.PRIVARY_TYPE_NAME_CHAR.equals(type)) {
				return "'" + ((char) val) + "'";
			}
			return Integer.toString(val);
		}
		if (constantPoolContent instanceof CONSTANT_String_info) {
			return "\""
					+ ConstantPoolParseUtil.getUtf8String(((CONSTANT_String_info) constantPoolContent).getNameIndex())
					+ "\"";
		}
		return "";
	}

	private static void getFieldDeclare(Map<String, String> map, ClassInfo classInfo, FieldInfo fieldInfo) {
		StringBuilder fieldDeclare = new StringBuilder();
		// ACC_PUBLIC,ACC_PRIVATE,ACC_PROTECTED 这三个只能有其中一个或没有
		boolean accessFlag = false;
		if (fieldInfo.isPublic()) {
			fieldDeclare.append("public");
			accessFlag = true;
		}
		if (fieldInfo.isProtected()) {
			if (accessFlag) {
				throw new RuntimeException("ACC_PUBLIC,ACC_PRIVATE,ACC_PROTECTED only one declare");
			}
			fieldDeclare.append("protected");
			accessFlag = true;
		}
		if (fieldInfo.isPrivate()) {
			if (accessFlag) {
				throw new RuntimeException("ACC_PUBLIC,ACC_PRIVATE,ACC_PROTECTED only one declare");
			}
			fieldDeclare.append("private");
			accessFlag = true;
		}
		// ACC_FINAL,ACC_VOLATILE 这两个只能有其中一个或没有
		if (fieldInfo.isFinal() && fieldInfo.isVolatile()) {
			throw new RuntimeException("ACC_FINAL,ACC_VOLATILE only one declare");
		}
		// Interface内的Field Info都是ACC_PUBLIC,ACC_STATIC,ACC_FINAL修饰
		if (classInfo.isInterface() && !(fieldInfo.isPublic() && fieldInfo.isStatic() && fieldInfo.isFinal())) {
			throw new RuntimeException("interface field must be ACC_PUBLIC and ACC_STATIC and ACC_FINAL");
		}

		if (fieldInfo.isStatic()) {
			fieldDeclare.append(" static");
		}

		if (fieldInfo.isFinal()) {
			fieldDeclare.append(" final");
		}

		if (fieldInfo.isVolatile()) {
			fieldDeclare.append(" volatile");
		}

		if (fieldInfo.isTransient()) {
			fieldDeclare.append(" transient");
		}

		if (fieldInfo.isEnum()) {
			fieldDeclare.append(" enum");
		}

		map.put(ACCESS_FLAG, fieldDeclare.toString());
	}

	private static void getFileTypeName(Map<String, String> map, FieldInfo fieldInfo) {
		map.put(FIELD_NAME, " " + fieldInfo.getFieldName());
		map.put(CLASS_TYPE, " " + fieldInfo.getFieldType());
	}
}

package com.harris.decompile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.ExceptionsAttrtube;
import com.harris.attrtube.SignatureAttrtube;
import com.harris.constant.Constant;
import com.harris.constant.pool.CONSTANT_Class_info;
import com.harris.info.ClassInfo;
import com.harris.info.MethodInfo;
import com.harris.parse.ConstantPoolParseUtil;
import com.harris.util.MapUtil;

public class MethodDecompile {
	private static final String ANNOTATIONS = "annotations";
	private static final String ACCESS_FLAG = "accessFlag";
	private static final String TYPE_PARAMETERS = "typeParameters";
	private static final String RETURN_TYPE = "returnType";
	private static final String METHOD_NAME = "methodName";
	public static final String PARAMETER_ANNOTATION = "paramAnnotation";
	private static final String METHOD_PARAMETER = "methodParameter";
	private static final String THROW_EXCEPTION = "throwException";
	private static final String METHOD_CODE = "methodCode";

	private static final String METHOD_FORMAT = "[annotations][accessFlag][typeParameters][returnType][methodName]([paramAnnotation][methodParameter])[throwException] {[methodCode]}";

	public static String decompile(ClassInfo classInfo) {
		if (classInfo.methodCount <= 0) {
			return "";
		}
		return decompile(classInfo, classInfo.methodInfos);
	}

	private static String decompile(ClassInfo classInfo, List<MethodInfo> methodInfos) {
		Map<String, String> map = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		for (MethodInfo methodInfo : methodInfos) {
			initMap(map);
			getAccessFlag(map, classInfo, methodInfo);
			getMethodNameReturnType(map, methodInfo);
			getAnnotations(map, methodInfo);
			getMethodParameter(map, methodInfo);
			sb.append(MapUtil.parseMap(map, METHOD_FORMAT));
		}
		return sb.toString();
	}

	private static void getMethodParameter(Map<String, String> map, MethodInfo methodInfo) {
		List<Map<String, String>> methodParameters = methodInfo.getMethodParameters();
		if (methodParameters == null || methodParameters.size() <= 0) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		for (Map<String, String> param : methodParameters) {
			for (Entry<String, String> entry : param.entrySet()) {
				sb.append(entry.getKey()).append(" ").append(entry.getValue());
			}
		}
		map.put(METHOD_PARAMETER, sb.toString());
	}

	private static void getAnnotations(Map<String, String> map, MethodInfo methodInfo) {
		if (methodInfo.attributesCount <= 0) {
			return;
		}
		for (Entry<String, AttrtubeInfo> entry : methodInfo.attrtubeInfos.entrySet()) {
			switch (entry.getKey()) {
			case AttrtubeInfo.Code:

				break;
			case AttrtubeInfo.Exceptions:
				ExceptionsAttrtube exceptionsAttrtube = (ExceptionsAttrtube) entry.getValue();
				MapUtil.putValue(map, THROW_EXCEPTION, getExceptions(exceptionsAttrtube));
				break;
			case AttrtubeInfo.Synthetic:
				break;
			case AttrtubeInfo.Signature:
				getSignature(map, (SignatureAttrtube) entry.getValue());
				break;
			case AttrtubeInfo.Deprecated:
				break;
			case AttrtubeInfo.RuntimeVisibleAnnotations:
				break;
			case AttrtubeInfo.RuntimeInvisibleAnnotations:
				break;
			case AttrtubeInfo.RuntimeInvisibleParameterAnnotations:
				break;
			case AttrtubeInfo.RuntimeVisibleParameterAnnotations:
				break;
			case AttrtubeInfo.RuntimeVisibleTypeAnnotations:
				break;
			case AttrtubeInfo.RuntimeInvisibleTypeAnnotations:
				break;
			case AttrtubeInfo.AnnotationDefault:
				break;
			case AttrtubeInfo.MethodParameters:
				break;
			default:
				break;
			}
		}
	}

	private static void getSignature(Map<String, String> map, SignatureAttrtube signatureAttrtube) {
		String signatrue = ConstantPoolParseUtil.getUtf8String(signatureAttrtube.signatureIndex);

	}

	private static String getExceptions(ExceptionsAttrtube exceptionsAttrtube) {
		if (exceptionsAttrtube.numberOfExceptions <= 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder(" throws");
		for (Integer exceptionIndex : exceptionsAttrtube.exceptionIndexTables) {
			CONSTANT_Class_info constant_Class_info = (CONSTANT_Class_info) ConstantPoolParseUtil
					.getConstantPoolContents().get(exceptionIndex);
			sb.append(constant_Class_info.getClassNameSignature()).append(",");
		}
		return sb.deleteCharAt(sb.length() - 1).toString();
	}

	private static void initMap(Map<String, String> map) {
		map.put(ANNOTATIONS, "");
		map.put(ACCESS_FLAG, "");
		map.put(TYPE_PARAMETERS, "");
		map.put(RETURN_TYPE, "");
		map.put(METHOD_NAME, "");
		map.put(METHOD_PARAMETER, "");
		map.put(THROW_EXCEPTION, "");
		map.put(METHOD_CODE, "");
	}

	private static void getAccessFlag(Map<String, String> map, ClassInfo classInfo, MethodInfo methodInfo) {
		// ACC_PUBLIC,ACC_PRIVATE,ACC_PROTECTED 这三个只能有其中一个或没有
		boolean accessFlag = false;
		StringBuilder methodDeclare = new StringBuilder();
		if (methodInfo.isPublic()) {
			methodDeclare.append("public");
			accessFlag = true;
		}
		if (methodInfo.isProtected()) {
			if (accessFlag) {
				throw new RuntimeException("ACC_PUBLIC,ACC_PRIVATE,ACC_PROTECTED only one declare");
			}
			methodDeclare.append("protected");
			accessFlag = true;
		}
		if (methodInfo.isPrivate()) {
			if (accessFlag) {
				throw new RuntimeException("ACC_PUBLIC,ACC_PRIVATE,ACC_PROTECTED only one declare");
			}
			methodDeclare.append("private");
			accessFlag = true;
		}
		if (classInfo.isInterface()) {
			if (methodInfo.isPublic() == methodInfo.isPrivate()) {
				throw new RuntimeException("interface method accessFlag only one of ACC_PUBLIC and ACC_PRIVATE");
			}
			// 在jdk版本小于52.0(1.8)之前，方法修饰符都必须有ACC_PUBLIC与ACC_ABSTRACT
			if (classInfo.versionNum < Constant.JDK_8_VERSION && !(methodInfo.isPublic() && methodInfo.isAbstract())) {
				throw new RuntimeException(String.format(
						"jdk version is : %d,interface method access flag must be ACC_PUBLIC and ACC_ABSTRACT",
						classInfo.versionNum));
			}
			// Interface 方法不能是ACC_PROTECTED, ACC_FINAL, ACC_SYNCHRONIZED,
			// ACC_NATIVE
			if (methodInfo.isProtected() || methodInfo.isFinal() || methodInfo.isSynchornized()
					|| methodInfo.isNative()) {
				throw new RuntimeException(
						"interface method access flag can't be ACC_PROTECTED or ACC_FINAL or ACC_SYNCHRONIZED or ACC_NATIVE");
			}
		}
		// ACC_ABSTRACT情况下不能是ACC_PRIVATE,ACC_STATIC,ACC_STRICT, ACC_FINAL,
		// ACC_SYNCHRONIZED,ACC_NATIVE
		if (methodInfo.isAbstract() && (methodInfo.isPrivate() || methodInfo.isStatic() || methodInfo.isFinal()
				|| methodInfo.isSynchornized() || methodInfo.isNative() || methodInfo.isStrict())) {
			throw new RuntimeException(
					" method access flag is abstract, other flag can't be ACC_PRIVATE or ACC_STATIC or ACC_STRICT  ");
		}
		if (methodInfo.isNative() && methodInfo.isStrict()) {
			throw new RuntimeException(" method access flag can't be ACC_NATIVE and ACC_STRICT");
		}
		if (methodInfo.isStatic()) {
			methodDeclare.append(" static");
		}
		if (methodInfo.isAbstract()) {
			methodDeclare.append(" abstract");
		}
		if (methodInfo.isFinal()) {
			methodDeclare.append(" final");
		}
		if (methodInfo.isSynchornized()) {
			methodDeclare.append(" synchornized");
		}
		if (methodInfo.isNative()) {
			methodDeclare.append(" native");
		}
		if (methodInfo.isStrict()) {
			methodDeclare.append(" strictfp");
		}

		MapUtil.putValue(map, ACCESS_FLAG, methodDeclare.toString());
	}

	private static void getMethodNameReturnType(Map<String, String> map, MethodInfo methodInfo) {
		map.put(RETURN_TYPE, " " + methodInfo.getReturnType());
		map.put(METHOD_NAME, " " + methodInfo.getMethodName());
	}

}

package com.harris.decompile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.InnerClassesAttrtube;
import com.harris.attrtube.InnerClassesAttrtube.InnerClassInfo;
import com.harris.attrtube.RuntimeInvisibleAnnotationsAttrtube;
import com.harris.attrtube.RuntimeVisibleAnnotationsAttrtube;
import com.harris.attrtube.SignatureAttrtube;
import com.harris.info.ClassInfo;
import com.harris.info.TypeInfo;
import com.harris.parse.ConstantPoolParseUtil;
import com.harris.util.MapUtil;

public class ClassDecompile {

	private static String OBJECT_CLASS_NAME = "java.lang.Object";

	private static final String ANNOTATIONS = "annotations";
	private static final String ACCESS_FLAG = "accessFlag";
	private static final String CLASS_TYPE = "classType";
	private static final String CLASS_NAME = "className";
	private static final String TYPE_PARAMETERS = "typeParameters";
	private static final String SUPER_CLASS = "superClass";
	private static final String INTERFACES = "interfaces";
	private static final String INNER_CLASS = "innerCLass";

	// [annotations]\n[accessFlag] [classType] [className][<typeParameters>]
	// [superClass] [interfaces] {\n[body]\n}
	private static final String CLASS_SIGNATURE_FORMAT = "[annotations][accessFlag][classType][className][typeParameters][superClass][interfaces]{\n%s\n}";

	public static void decompile(ClassInfo classInfo) {
		String decompileClass = decompileClass(classInfo);
		String field = FieldDecompile.decompile(classInfo);
		String method = MethodDecompile.decompile(classInfo);
		System.out.println(String.format(decompileClass, field));
	}

	public static String decompileClass(ClassInfo classInfo) {
		Map<String, String> map = new LinkedHashMap<>();
		map.put(ANNOTATIONS, "");
		map.put(ACCESS_FLAG, "");
		map.put(CLASS_TYPE, "");
		map.put(CLASS_NAME, "");
		map.put(TYPE_PARAMETERS, "");
		map.put(SUPER_CLASS, "");
		map.put(INTERFACES, "");
		decompileCLassDeclare(classInfo, map);
		decompileAttrtube(classInfo, map);
		return MapUtil.parseMap(map, CLASS_SIGNATURE_FORMAT);

	}

	public static void decompileCLassDeclare(ClassInfo classInfo, Map<String, String> map) {
		String accessFlag = "", classType = "class", superClass = "", interfaces = "";

		// ACC_ABSTRACT 跟 ACC_FINAL 不能同时设置
		if (classInfo.isAbstract() && classInfo.isFinal()) {
			throw new RuntimeException("ACC_ABSTRACT and ACC_FINAL can't be set at same time");
		}

		// 类声明
		if (classInfo.isPublic()) {
			accessFlag = "public ";
		}

		if (classInfo.isAbstract()) {
			accessFlag += " abstract";
		}

		if (accessFlag.length() > 0) {
			MapUtil.putValue(map, ACCESS_FLAG, accessFlag);
		}

		// 接口类ACC_INTERFACE， 一定有ACC_ABSTRACT，且不能是ACC_FINAL或ACC_SUPER或ACC_ENUM
		if (classInfo.isInterface()) {
			if (!classInfo.isAbstract()) {
				throw new RuntimeException("interface class access flag must be ACC_ABSTRACT");
			}
			if (classInfo.isFinal() || classInfo.isSuper() || classInfo.isEnum()) {
				throw new RuntimeException(
						"interface class access flag can't be ACC_FINAL or ACC_SUPER or ACC_ABSTRACT");
			}
			classType = "interface";
		}

		// 注解类ACC_ANNOTATION，同时必须设置ACC_INTERFACE
		if (classInfo.isAnnotation()) {
			if (!classInfo.isInterface()) {
				throw new RuntimeException("annotation class flag must be ACC_INTERFACE");
			}
			classType = " @interface";
		}

		if (classInfo.isEnum()) {
			classType = "enum";
		}
		if (classType.length() > 0) {
			MapUtil.putValue(map, CLASS_TYPE, classType);
		}

		MapUtil.putValue(map, CLASS_NAME, " " + classInfo.getThisClassName());
		// 父类为Object，不需体现打印
		if (classInfo.superClassIndex > 0) {
			String superClassName = classInfo.getSuperClassName();
			if (!OBJECT_CLASS_NAME.equals(superClassName)) {
				superClass = " extends " + superClassName;
			}
		}
		if (superClass.length() > 0) {
			MapUtil.putValue(map, SUPER_CLASS, superClass);
		}
		if (classInfo.interfaceCount > 0) {
			StringBuilder classDeclare = new StringBuilder();
			classDeclare.append(classInfo.isInterface() ? " extends" : "implement ");
			for (int i = 0; i < classInfo.interfaceCount; i++) {
				classDeclare.append(ClassInfo.getClassName(classInfo.interfaceInfos.get(i))).append(",");
			}
			classDeclare.deleteCharAt(classDeclare.length() - 1);
			interfaces = classDeclare.toString();
		}
		if (interfaces.length() > 0) {
			MapUtil.putValue(map, INTERFACES, interfaces);
		}
	}

	private static void decompileAttrtube(ClassInfo classInfo, Map<String, String> map) {
		if (classInfo.attrtubeCount <= 0) {
			return;
		}
		for (Entry<String, AttrtubeInfo> entry : classInfo.attrtubeInfos.entrySet()) {
			switch (entry.getKey()) {
			case AttrtubeInfo.InnerClasses:

				break;
			case AttrtubeInfo.Signature:
				SignatureAttrtube signatureAttrtube = (SignatureAttrtube) entry.getValue();
				parseSignature(map, signatureAttrtube);
				break;
			case AttrtubeInfo.RuntimeVisibleAnnotations:
				RuntimeVisibleAnnotationsAttrtube runtimeVisibleAnnotationsAttrtube = (RuntimeVisibleAnnotationsAttrtube) entry
						.getValue();
				MapUtil.putValue(map, ANNOTATIONS,
						AnnotationDecompile.decompileAnnotation(runtimeVisibleAnnotationsAttrtube.annotations));
				break;
			case AttrtubeInfo.RuntimeInvisibleAnnotations:
				RuntimeInvisibleAnnotationsAttrtube runtimeInvisibleAnnotationsAttrtube = (RuntimeInvisibleAnnotationsAttrtube) entry
						.getValue();
				MapUtil.putValue(map, ANNOTATIONS,
						AnnotationDecompile.decompileAnnotation(runtimeInvisibleAnnotationsAttrtube.annotations));
				break;
			case AttrtubeInfo.RuntimeVisibleTypeAnnotations:

				break;
			case AttrtubeInfo.RuntimeInvisibleTypeAnnotations:

				break;
			case AttrtubeInfo.Deprecated:
//				putValue(map, ANNOTATIONS, "@Deprecated\n");
				break;
			default:
				break;
			}
		}
	}

	private static void parseSignature(Map<String, String> map, SignatureAttrtube signatureAttrtube) {
		String s = ConstantPoolParseUtil.getUtf8String(signatureAttrtube.signatureIndex);
		// 解析类签名
//		ClassSignature:
//			[TypeParameters] SuperclassSignature {SuperinterfaceSignature}
//			TypeParameters:
//			< TypeParameter {TypeParameter} >
//			TypeParameter:
//			Identifier ClassBound {InterfaceBound}
//			ClassBound:
//			: [ReferenceTypeSignature]
//			InterfaceBound:
//			: ReferenceTypeSignature
//			SuperclassSignature:
//			ClassTypeSignature
//			SuperinterfaceSignature:
//			ClassTypeSignature
		// 例如<T:Ljava/lang/Object;K:Ljava/lang/Object;>Ljava/lang/Object
		String[] paramTypes = s.split(">")[0].substring(1).split(";");
		if (paramTypes == null || paramTypes.length <= 0) {
			return;
		}
		StringBuilder sb = new StringBuilder("<");
		for (String type : paramTypes) {
			if (type.length() <= 0) {
				continue;
			}
			String[] split = type.split(":");
			sb.append(split[0]);
			split[1] = TypeInfo.getFieldTypeByDescription(split[1]);
			if (!OBJECT_CLASS_NAME.equals(split[1])) {
				sb.append(" extends ").append(split[1]);
			}
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1).append(">");
		map.put(TYPE_PARAMETERS, sb.toString());
	}

	private static Map<String, String> parseInnerClass(InnerClassesAttrtube innerClassesAttrtube) {
		Map<String, String> map = new HashMap<>();
		if (innerClassesAttrtube.numberOfClasses <= 0) {
			return map;
		}
		List<String> innerClassList = new ArrayList<>(innerClassesAttrtube.numberOfClasses);
		for (InnerClassInfo inner : innerClassesAttrtube.innerClassInfos) {

		}
		return null;
	}
}

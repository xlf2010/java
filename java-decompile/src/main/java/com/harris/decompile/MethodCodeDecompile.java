package com.harris.decompile;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.CodeAttrtube;
import com.harris.attrtube.LocalVariableTableAttrtube;
import com.harris.attrtube.LocalVariableTableAttrtube.LocalVariableTable;
import com.harris.attrtube.LocalVariableTypeTableAttrtube;
import com.harris.constant.bytecode.ComparisonsByteCode;
import com.harris.constant.bytecode.ConstantByteCode;
import com.harris.constant.bytecode.ControlByteCode;
import com.harris.constant.bytecode.ConversionsByteCode;
import com.harris.constant.bytecode.ExtendedByteCode;
import com.harris.constant.bytecode.LoadByteCode;
import com.harris.constant.bytecode.MathByteCode;
import com.harris.constant.bytecode.ReferencesByteCode;
import com.harris.constant.bytecode.ReservedByteCode;
import com.harris.constant.bytecode.StackByteCode;
import com.harris.constant.bytecode.StoreByteCode;
import com.harris.info.ClassInfo;
import com.harris.info.MethodInfo;

public class MethodCodeDecompile {

	public static final short max_Constants_index = ConstantByteCode._ldc2_w;
	public static final short max_Loads_index = LoadByteCode._saload;
	public static final short max_Stores_index = StoreByteCode._sastore;
	public static final short max_Stack_index = StackByteCode._swap;
	public static final short max_Math_index = MathByteCode._iinc;
	public static final short max_Conversions_index = ConversionsByteCode._i2s;
	public static final short max_Comparisons_index = ComparisonsByteCode._if_acmpne;
	public static final short max_Control_index = ControlByteCode._return;
	public static final short max_References_index = ReferencesByteCode._monitorexit;
	public static final short max_Extended_index = ExtendedByteCode._jsr_w;
	public static final short max_Reserved_index = ReservedByteCode._impdep2;

	public static void decompileCode(ClassInfo classInfo, MethodInfo methodInfo, CodeAttrtube codeAttrtube) {
		if (codeAttrtube.exceptionTableLength > 0) {

		}
		// 操作数栈
		Deque<String> opStack = new ArrayDeque<>(codeAttrtube.maxStack);
		// 局部变量名
		String[] localVariable = new String[codeAttrtube.maxStack];
		// List<String> localVariable = new ArrayList<>(codeAttrtube.maxLocals);
		// 局部变量类型
		String[] localVariableType = new String[codeAttrtube.maxLocals];

		int length = codeAttrtube.maxLocals;
		// 解析方法参数,非静态方法生成this参数
//		if (!methodInfo.isStatic()) {
//			localVariable[0] = "this";
//			length--;
//		}
//		for (int i = 0; i < length; i++) {
//			localVariable[i + 1] = ("arg" + i);
//		}
		int nextInstruction = 0;
		while (nextInstruction < codeAttrtube.codeLength) {

		}
	}

	private static void parseAttrtube(Map<String, AttrtubeInfo> attrtubeInfo) {
		for (Entry<String, AttrtubeInfo> entry : attrtubeInfo.entrySet()) {
			switch (entry.getKey()) {
			case AttrtubeInfo.StackMapTable:
				break;
			case AttrtubeInfo.LineNumberTable:
				break;
			case AttrtubeInfo.LocalVariableTable:
				LocalVariableTableAttrtube localVariableTableAttrtube = (LocalVariableTableAttrtube) entry.getValue();
				break;
			case AttrtubeInfo.LocalVariableTypeTable:
				LocalVariableTypeTableAttrtube localVariableTypeTableAttrtube = (LocalVariableTypeTableAttrtube) entry
						.getValue();
				break;
			default:
				break;
			}
		}
	}

	// 获取局部变量名称跟类型，以';'切分，前面为变量名，后面为类型，如localVar1;java.lang.String
	private List<String> getLocalVariableNameType(Map<String, AttrtubeInfo> attrtubeInfo, int maxLocals) {

		List<String> list = new ArrayList<>(maxLocals);
		if (maxLocals <= 0) {
			return list;
		}
		if (attrtubeInfo.containsKey(AttrtubeInfo.LocalVariableTable)) {
			LocalVariableTableAttrtube localVariableTableAttrtube = (LocalVariableTableAttrtube) attrtubeInfo
					.get(AttrtubeInfo.LocalVariableTable);
		}

		return list;
	}

	private static List<String> getLocalVariableNameByLocalVariableTable(
			LocalVariableTableAttrtube localVariableTableAttrtube) {
		List<String> list = new ArrayList<>(localVariableTableAttrtube.localVariableTableLength);
		if (localVariableTableAttrtube.localVariableTableLength <= 0) {
			return list;
		}
		for (LocalVariableTable localVariableTable : localVariableTableAttrtube.loVariableTables) {

		}
		return list;
	}

	private static String getlocalVarName(List<String> localVariableList, int index) {
		if (index >= localVariableList.size()) {
			return "";
		}
		return localVariableList.get(index).split(";")[0];
	}

	private static String getlocalVarType(List<String> localVariableList, int index) {
		if (index >= localVariableList.size()) {
			return "";
		}
		return localVariableList.get(index).split(";")[1];
	}

	private static String[] prepareLocalVariableName(MethodInfo methodInfo, CodeAttrtube codeAttrtube) {
		String[] localVariableName = new String[codeAttrtube.maxLocals];
		if (codeAttrtube.codeLength <= 0) {
			return localVariableName;
		}
		if (codeAttrtube.attrtubeInfos.containsKey(AttrtubeInfo.LocalVariableTable)) {

		}
		return null;
	}

	private int parseByteCode(Deque<String> opStack, short code) {

		if (code < max_Constants_index) {

		}
		return code;
	}
}

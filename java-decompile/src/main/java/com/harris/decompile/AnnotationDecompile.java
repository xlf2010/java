package com.harris.decompile;

import java.util.List;

import com.harris.attrtube.AttrtubeInfo;
import com.harris.attrtube.RuntimeInvisibleAnnotationsAttrtube;
import com.harris.attrtube.RuntimeVisibleAnnotationsAttrtube;
import com.harris.constant.pool.CONSTANT_Integer_info;
import com.harris.info.AnnotationInfo;
import com.harris.info.AnnotationInfo.ArrayValue;
import com.harris.info.AnnotationInfo.ElementValue;
import com.harris.info.AnnotationInfo.ElementValuePair;
import com.harris.info.AnnotationInfo.EnumConstValue;
import com.harris.parse.ConstantPoolParseUtil;
import com.harris.info.TypeInfo;

public class AnnotationDecompile {
	public static String decompile(String name, AttrtubeInfo attrtubeInfo) {
		switch (name) {
		case AttrtubeInfo.RuntimeVisibleAnnotations:
			RuntimeVisibleAnnotationsAttrtube runtimeVisibleAnnotationsAttrtube = (RuntimeVisibleAnnotationsAttrtube) attrtubeInfo;
			return decompileAnnotation(runtimeVisibleAnnotationsAttrtube.annotations);
		case AttrtubeInfo.RuntimeInvisibleAnnotations:
			RuntimeInvisibleAnnotationsAttrtube runtimeInvisibleAnnotationsAttrtube = (RuntimeInvisibleAnnotationsAttrtube) attrtubeInfo;
			return decompileAnnotation(runtimeInvisibleAnnotationsAttrtube.annotations);
		case AttrtubeInfo.RuntimeVisibleParameterAnnotations:

			break;
		case AttrtubeInfo.RuntimeInvisibleParameterAnnotations:

			break;
		case AttrtubeInfo.RuntimeVisibleTypeAnnotations:

			break;
		case AttrtubeInfo.RuntimeInvisibleTypeAnnotations:

			break;
		default:
			break;
		}
		return name;
	}

	public static String decompileAnnotation(List<AnnotationInfo> annotationInfos) {
		if (annotationInfos.size() <= 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (AnnotationInfo annotationInfo : annotationInfos) {
			sb.append("@").append(TypeInfo.getFieldType(annotationInfo.type_index));
			if (annotationInfo.num_element_value_pairs > 0) {
				sb.append("(").append(decompileAnnotationValue(annotationInfo.elementValuePairs)).append(")");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	private static String decompileAnnotationValue(List<ElementValuePair> elementValuePairs) {
		StringBuilder sb = new StringBuilder();
		// 解析key，value
		for (ElementValuePair elementValuePair : elementValuePairs) {
			sb.append(ConstantPoolParseUtil.getUtf8String(elementValuePair.element_name_index)).append("=")
					.append(decompileElementValue(elementValuePair.elementValue)).append(",");
		}
		return sb.deleteCharAt(sb.length() - 1).toString();
	}

	private static String decompileElementValue(ElementValue elementValue) {
		String valueString = "";
		CONSTANT_Integer_info constant_Integer_info = null;
		switch (elementValue.tag) {
		case AnnotationInfo.ELEMENTVALUE_TAG_B:
			constant_Integer_info = (CONSTANT_Integer_info) ConstantPoolParseUtil.getConstantPoolContents()
					.get(elementValue.value.const_value_index);
			valueString = Byte.toString((byte) constant_Integer_info.getIntVal());
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_I:
			constant_Integer_info = (CONSTANT_Integer_info) ConstantPoolParseUtil.getConstantPoolContents()
					.get(elementValue.value.const_value_index);
			valueString = Integer.toString(constant_Integer_info.getIntVal());
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_Z:
			constant_Integer_info = (CONSTANT_Integer_info) ConstantPoolParseUtil.getConstantPoolContents()
					.get(elementValue.value.const_value_index);
			valueString = constant_Integer_info.getIntVal() == 1 ? "true" : "false";
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_J:
		case AnnotationInfo.ELEMENTVALUE_TAG_D:
		case AnnotationInfo.ELEMENTVALUE_TAG_F:
		case AnnotationInfo.ELEMENTVALUE_TAG_S:
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_s:
			valueString = "\"" + ConstantPoolParseUtil.getUtf8String(elementValue.value.const_value_index) + "\"";
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_C:
			valueString = "'" + ConstantPoolParseUtil.getUtf8String(elementValue.value.const_value_index) + "'";
			break;

		case AnnotationInfo.ELEMENTVALUE_TAG_c:
			valueString = TypeInfo.getFieldType(elementValue.value.class_info_index) + ".class";
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_e:
			EnumConstValue enumConstValue = elementValue.value.enum_const_value;
			valueString = TypeInfo.getFieldType(enumConstValue.type_name_index) + "."
					+ ConstantPoolParseUtil.getUtf8String(enumConstValue.const_name_index);
			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_ANNOTATION:

			break;
		case AnnotationInfo.ELEMENTVALUE_TAG_ARRAY:
			ArrayValue arrayValue = elementValue.value.arrayValue;
			StringBuilder sb = new StringBuilder(valueString);
			if (arrayValue.num_values > 0) {
				for (int i = 0; i < arrayValue.num_values; i++) {
					sb.append(decompileElementValue(arrayValue.values.get(i))).append(",");
				}
			}
			// 非空数组删除最后一个逗号
			if (sb.length() > 0) {
				sb.deleteCharAt(sb.length() - 1);
			}
			valueString = "{" + sb.toString() + "}";
			break;
		default:
			break;
		}
		return valueString;
	}
}

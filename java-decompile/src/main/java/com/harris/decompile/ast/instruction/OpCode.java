package com.harris.decompile.ast.instruction;

import com.harris.constant.bytecode.ConstantByteCode;
import com.harris.constant.bytecode.ControlByteCode;
import com.harris.constant.bytecode.LoadByteCode;
import com.harris.constant.bytecode.OperandLength;
import com.harris.constant.bytecode.OperandType;
import com.harris.constant.bytecode.ReferencesByteCode;
import com.harris.constant.bytecode.StackBehavior;
import com.harris.constant.bytecode.StoreByteCode;

public class OpCode {
	// 操作码
	public short opCode;
	// 指令名称
	public String codeName;
	// 操作数长度
	public int operandLength;
	// 操作数类型
	public int operandType;
	// 入栈行为
	public int stackPushBehavior;
	// 出栈行为
	public int stackPopBehavior;

	public OpCode() {
	}

	public OpCode(short opCode, String codeName, int operandLength, int operandType, int stackPushBehavior,
			int stackPopBehavior) {
		super();
		this.opCode = opCode;
		this.codeName = codeName;
		this.operandLength = operandLength;
		this.operandType = operandType;
		this.stackPushBehavior = stackPushBehavior;
		this.stackPopBehavior = stackPopBehavior;
	}

	public static final OpCode[] OP_CODES = new OpCode[256];

	static {
//        0: aload_0
//        1: invokespecial #8                  // Method java/lang/Object."<init>":()V
//        4: return

		// test MainTest.<init>
		OP_CODES[LoadByteCode._aload_0] = new OpCode(LoadByteCode._aload_0, LoadByteCode.s_aload_0,
				OperandLength._0_BYTE, OperandType.non, StackBehavior.push_1, StackBehavior.pop_0);
		OP_CODES[ReferencesByteCode._invokespecial] = new OpCode(ReferencesByteCode._invokespecial,
				ReferencesByteCode.s_invokespecial, OperandLength._2_BYTE, OperandType.Methodref,
				StackBehavior.push_unknown, StackBehavior.pop_unknown);
		OP_CODES[ControlByteCode._return] = new OpCode(ControlByteCode._return, ControlByteCode.s_return,
				OperandLength._0_BYTE, OperandType.non, StackBehavior.push_0, StackBehavior.pop_0);

		// test MainTest.main
//		     0: iconst_1
//	         1: istore_1
//	         2: getstatic     #16                 // Field java/lang/System.out:Ljava/io/PrintStream;
//	         5: iload_1
//	         6: invokevirtual #22                 // Method java/io/PrintStream.println:(I)V
//	         9: return

		OP_CODES[ConstantByteCode._iconst_1] = new OpCode(ConstantByteCode._iconst_1, ConstantByteCode.s_iconst_1,
				OperandLength._0_BYTE, OperandType.non, StackBehavior.push_i4, StackBehavior.pop_0);
		OP_CODES[StoreByteCode._istore_1] = new OpCode(StoreByteCode._istore_1, StoreByteCode.s_istore_1,
				OperandLength._0_BYTE, OperandType.non, StackBehavior.push_0, StackBehavior.pop_i4);
		OP_CODES[ReferencesByteCode._getstatic] = new OpCode(ReferencesByteCode._getstatic,
				ReferencesByteCode.s_getstatic, OperandLength._2_BYTE, OperandType.Fieldref, StackBehavior.push_1,
				StackBehavior.pop_0);
		OP_CODES[LoadByteCode._iload_1] = new OpCode(LoadByteCode._iload_1, LoadByteCode.s_iload_1,
				OperandLength._0_BYTE, OperandType.non, StackBehavior.push_1, StackBehavior.pop_0);
		OP_CODES[ReferencesByteCode._invokevirtual] = new OpCode(ReferencesByteCode._invokevirtual,
				ReferencesByteCode.s_invokevirtual, OperandLength._2_BYTE, OperandType.Methodref,
				StackBehavior.push_unknown, StackBehavior.pop_unknown);

	}

	public boolean isSwitch(short opCode) {
		switch (opCode) {
		case ControlByteCode._lookupswitch:
		case ControlByteCode._tableswitch:
			return true;
		default:
			break;
		}
		return false;
	}

}

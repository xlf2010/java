package com.harris.decompile.ast.instruction;

public class ByteCodeInstruction {
	// 字节码位于数组中的位置
	public int offset;
	// 操作码
	public OpCode opCode;
	// 操作数
	public byte[] operand;
	// 操作数实际长度
	public int operandLength;
	// 上一条指令
	public ByteCodeInstruction pre;
	// 下一条指令
	public ByteCodeInstruction next;

	@Override
	public String toString() {
		return opCode.codeName;
	}
}

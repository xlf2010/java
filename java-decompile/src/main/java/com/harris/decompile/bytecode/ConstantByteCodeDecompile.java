package com.harris.decompile.bytecode;

import java.util.Deque;

import com.harris.constant.bytecode.ConstantByteCode;
import com.harris.constant.pool.CONSTANT_Class_info;
import com.harris.constant.pool.CONSTANT_Double_info;
import com.harris.constant.pool.CONSTANT_Float_info;
import com.harris.constant.pool.CONSTANT_Integer_info;
import com.harris.constant.pool.CONSTANT_Long_info;
import com.harris.constant.pool.CONSTANT_MethodHandle_info;
import com.harris.constant.pool.CONSTANT_String_info;
import com.harris.constant.pool.ConstantPoolContent;
import com.harris.info.TypeInfo;
import com.harris.parse.ConstantPoolParseUtil;

public class ConstantByteCodeDecompile {
	public static int decompile(Deque<String> opStack, short[] codes, int currentInstruction) {
		short code = codes[currentInstruction];
		switch (code) {
		case ConstantByteCode._nop:
			currentInstruction++;
			break;
		case ConstantByteCode._aconst_null:
			opStack.push("null");
			currentInstruction++;
			break;
		case ConstantByteCode._iconst_m1:
			opStack.push("-1");
			currentInstruction++;
			break;
		case ConstantByteCode._iconst_1:
			opStack.push("1");
			currentInstruction++;
			break;
		case ConstantByteCode._iconst_2:
			opStack.push("2");
			currentInstruction++;
			break;
		case ConstantByteCode._iconst_3:
			opStack.push("3");
			currentInstruction++;
			break;
		case ConstantByteCode._iconst_4:
			opStack.push("4");
			currentInstruction++;
			break;
		case ConstantByteCode._iconst_5:
			opStack.push("5");
			currentInstruction++;
			break;
		case ConstantByteCode._lconst_0:
			opStack.push("0L");
			currentInstruction++;
			break;
		case ConstantByteCode._lconst_1:
			opStack.push("1L");
			currentInstruction++;
			break;
		case ConstantByteCode._fconst_0:
			opStack.push("0.0F");
			currentInstruction++;
			break;
		case ConstantByteCode._fconst_1:
			opStack.push("1.0F");
			currentInstruction++;
			break;
		case ConstantByteCode._fconst_2:
			opStack.push("2.0F");
			currentInstruction++;
			break;
		case ConstantByteCode._dconst_0:
			opStack.push("0.0");
			currentInstruction++;
			break;
		case ConstantByteCode._dconst_1:
			opStack.push("1.0");
			currentInstruction++;
			break;
		case ConstantByteCode._bipush:
			opStack.push(Integer.toString((int) ((byte) codes[currentInstruction + 1])));
			currentInstruction += 2;
			break;
		case ConstantByteCode._sipush:
			int val = (((byte) codes[currentInstruction + 1]) << 8 | ((byte) codes[currentInstruction + 2]));
			opStack.push(Integer.toString(val));
			currentInstruction += 3;
			break;
		case ConstantByteCode._ldc:
			int ldc_index = ((byte) codes[currentInstruction + 1]);
			opStack.push(getConstantValue(ldc_index));
			currentInstruction += 2;
			break;
		case ConstantByteCode._ldc_w:
			int ldc_w_index = (((byte) codes[currentInstruction + 1]) << 8 | ((byte) codes[currentInstruction + 2]));
			opStack.push(getConstantValue(ldc_w_index));
			currentInstruction += 3;
			break;
		case ConstantByteCode._ldc2_w:
			int ldc2_w_index = (((byte) codes[currentInstruction + 1]) << 8 | ((byte) codes[currentInstruction + 2]));
			opStack.push(getConstantValueL2dc(ldc2_w_index));
			currentInstruction += 3;
			break;
		default:
			break;
		}
		return currentInstruction;
	}

	private static String getConstantValue(int index) {
		ConstantPoolContent constantPoolContent = ConstantPoolParseUtil.getConstantPoolContents().get(index);
		if (constantPoolContent instanceof CONSTANT_Integer_info) {
			return Integer.toString(((CONSTANT_Integer_info) constantPoolContent).getIntVal());
		}
		if (constantPoolContent instanceof CONSTANT_Float_info) {
			return Float.toString(((CONSTANT_Float_info) constantPoolContent).getFloatVal());
		}
		if (constantPoolContent instanceof CONSTANT_String_info) {
			CONSTANT_String_info constant_String_info = (CONSTANT_String_info) constantPoolContent;
			return ConstantPoolParseUtil.getUtf8String(constant_String_info.getNameIndex());
		}
		if (constantPoolContent instanceof CONSTANT_Class_info) {
			return TypeInfo.getFieldType(((CONSTANT_Class_info) constantPoolContent).getNameIndex()) + ".class";
		}

		if (constantPoolContent instanceof CONSTANT_MethodHandle_info) {
			
		}
		throw new RuntimeException(
				" ldc or ldc_w instruction unsupport constant value type,constant pool index is :" + index);
	}

	private static String getConstantValueL2dc(int index) {
		ConstantPoolContent constantPoolContent = ConstantPoolParseUtil.getConstantPoolContents().get(index);
		if (constantPoolContent instanceof CONSTANT_Double_info) {
			return Double.toString(((CONSTANT_Double_info) constantPoolContent).getDoubleVal());
		}
		if (constantPoolContent instanceof CONSTANT_Long_info) {
			return Long.toString(((CONSTANT_Long_info) constantPoolContent).getLongVal());
		}
		throw new RuntimeException("ldc2_w instruction unsupport constant value type,constant pool index is :" + index);
	}

}

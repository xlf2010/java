package com.harris;

import java.lang.reflect.Field;

import com.harris.constant.bytecode.ConstantByteCode;
import com.harris.constant.bytecode.ControlByteCode;
import com.harris.constant.bytecode.ConversionsByteCode;
import com.harris.constant.bytecode.ExtendedByteCode;
import com.harris.constant.bytecode.LoadByteCode;
import com.harris.constant.bytecode.MathByteCode;
import com.harris.constant.bytecode.ReferencesByteCode;
import com.harris.constant.bytecode.ReservedByteCode;
import com.harris.constant.bytecode.StackByteCode;
import com.harris.constant.bytecode.StoreByteCode;

public class TestLoop {
	private int[] is = { 1, 2, 31, 100, 231 };

	public void f1() {

		int i = 0;
		while (i < is.length) {
			System.out.println(i);
			i += 2;
		}

	}

	public void w1() {
		int i = 0;
		for (; i < is.length; i += 2) {
			System.out.println(i);
		}
	}

	public void dw1() {
		int i = 0;
		do {
			System.out.println(i);
			i++;
		} while (i < is.length);
	}

	public void if1(int i) {
		if (i < 10) {
			i = 20;
		} else {
			i = 30;
		}
	}

	public static void main(String[] args) {
		Class<?>[] classes = { ConstantByteCode.class, ControlByteCode.class, ConversionsByteCode.class,
				ExtendedByteCode.class, LoadByteCode.class, MathByteCode.class, ReferencesByteCode.class,
				ReservedByteCode.class, StackByteCode.class, StoreByteCode.class };
		for (int i = 0; i < classes.length; i++) {
			System.out.println(classes[i].getSimpleName());
			Field[] fieldInfos = classes[i].getFields();
			for (int j = 0; j < fieldInfos.length; j++) {
				if (fieldInfos[j].getClass().equals(String.class) || fieldInfos[j].getName().startsWith("s")) {
					continue;
				}
				System.out.println("public static final String s" + fieldInfos[j].getName() + "          =    \""
						+ fieldInfos[j].getName().substring(1) + "\"   ;");

			}
		}
	}
}

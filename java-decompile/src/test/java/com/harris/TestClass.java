package com.harris;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.io.FileReader;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.annotation.Resource.AuthenticationType;
import javax.print.attribute.IntegerSyntax;

@FunctionalInterface
interface A {
//	public A() {
//		long b1 = Long.MAX_VALUE;
//		try {
//			wait(b1, 20);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//	}

//	@Override
//	public boolean equals(Object obj) {
//		return false;
//	}
	void say();
}

@Target({ TYPE, FIELD, METHOD })
@Retention(RUNTIME)
@interface ABV {
	String[] value();

	ElementType[] elementTypes();

	boolean[] bs();

	int intval() default 10;
//	Retention tag();
}

@Deprecated
@Resource(name = "hello", lookup = "hahasf", type = Integer.class, authenticationType = AuthenticationType.APPLICATION, shareable = true)
@ABV(value = { "123", "1235436546", "235ksdn" }, intval = 1, elementTypes = {}, bs = { true, false })
public class TestClass<T extends IntegerSyntax, K extends Number> {
	@Deprecated
	private final K[][] k = null;
	@Resource(name = "boo")
	private static final boolean[] b = new boolean[1];
	public static final int i = 1;

	private static final float ff = 0.03f;

	public static void main(String[] args) throws Exception {
		int[] list = new int[10];
		list[3] = 3;
		setArray(list);
		System.out.println(Arrays.toString(list));
		System.out.println("Hello world");
	}

	public static void setArray(int[] it) {
		it[1] = 5;
		for (int i = 0; i < it.length; i++) {
			int j = it[i];
			i = j;
		}
		boolean v12 = false;
		int cc = 0;
		System.out.println(v12 + "" + cc);
	}

	A a = () -> {
		System.out.println("1");
	};

	public <A extends Number> void get(List<? extends Number> list, List<? super Number> list2, String... strings)
			throws Exception {
		Object number = list.get(0);
		System.out.println(number);
		Class<FileReader> class1 = FileReader.class;
		a.say();

	}

	public boolean test1(List<?> list) {
		return false;
	}
}

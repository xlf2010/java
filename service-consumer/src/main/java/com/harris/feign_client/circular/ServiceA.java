package com.harris.feign_client.circular;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
public class ServiceA {

	private ServiceB serviceB;

	public ServiceA() {
	}

	public ServiceA(ServiceB serviceB) {
		this.serviceB = serviceB;
	}

//	@Autowired
	public void setServiceB(ServiceB serviceB) {
		this.serviceB = serviceB;
	}

	public void serviceA() {
		serviceB.hashCode();
	}
}

package com.harris.feign_client.circular;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceB {

	private ServiceA serviceA;

	public ServiceB() {
	}

	public ServiceB(ServiceA serviceA) {
		this.serviceA = serviceA;
	}

	@Autowired
	public void setServiceA(ServiceA serviceA) {
		this.serviceA = serviceA;
	}

	public void serviceB() {
		serviceA.serviceA();
	}
}

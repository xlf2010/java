package com.harris.feign_client;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.harris.feign_client.circular.ServiceA;

/**
 * Hello world!
 *
 */
//@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@RestController
//@Scope("prototype")
public class FeignClientApp {

	@Resource(name = "service-providerFeignClient")
	private FeignTest feignTest;

	@Resource
	private ApplicationContext applicationContext;

	@Resource
	private ServiceA serviceA;

	@PostConstruct
	public void execute() {
		System.out.println("get feign result:" + feignTest.get());
	}

	@RequestMapping("/get")
	public String get(HttpServletRequest request) {
		System.out.println("current thread : "
				+ Thread.currentThread().getName() + " serviceA hash Code :"
				+ serviceA.hashCode());
		System.out.println("this hash code :" + hashCode());
		System.out.println("request remote addr " + request.getRemoteHost());
		return feignTest.get();
	}

	public static void main(String[] args) {
		SpringApplication.run(FeignClientApp.class, args);
	}
}

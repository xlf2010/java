package com.harris.feign_client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.harris.feign_client.circular.ServiceA;

@Component
public class TestFeignFallBackFactory implements FeignTest {

	@Autowired
	private ServiceA serviceA;

	@Override
	public String get() {
		System.out.println("fallback thread : "
				+ Thread.currentThread().getName() + " serviceA hash Code :"
				+ serviceA.hashCode());
		
		return "invoke error,fall back";
	}

}

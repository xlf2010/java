package com.harris.feign_client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(value = "${test.service.provider}", fallback = TestFeignFallBackFactory.class)
public interface FeignTest {

	@PostMapping(path = "/get")
	public String get();

}
